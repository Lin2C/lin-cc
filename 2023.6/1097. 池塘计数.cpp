#include<iostream>
using namespace std;
const int N = 1010 , M = N * N;

typedef pair<int,int> PII;
bool st[N][N];
PII q[M];
char g[N][N];
int n , m;
#define x first
#define y second


void bfs(int sx,int sy)
{
    int tt = -1  , hh = 0;
    q[++tt] = {sx , sy};
    st[sx][sy] = true;
    while(hh <= tt)
    {
        PII t = q[hh++];
        for(int i =  t.x - 1 ; i <= t.x + 1 ; i++)
            for(int j = t.y - 1; j <= t.y + 1;  j++)
            {
                if(i == t.x && j == t.y) continue;
                if(i < 0 || i >= n  || j < 0 || j >= m) continue;
                if(st[i][j]) continue;
                if(g[i][j] == '.') continue;
                q[++tt] = {i , j };
                st[i][j] = true;
            }
    }
}

int main()
{
    cin >> n >> m ;
    for(int i = 0 ;  i< n  ; i ++)
            scanf("%s",&g[i]);
    
    int cnt = 0 ;
    for(int i = 0 ;  i < n ;  i++)
        for(int j =  0 ; j < m ; j++)
            if(!st[i][j] && g[i][j] == 'W')
            {
                cnt ++;
                bfs(i,j);
            }
    printf("%d\n" , cnt);
    return 0;
}
