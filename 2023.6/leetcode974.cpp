class Solution {
public:
    int subarraysDivByK(vector<int>& nums, int k) {
        unordered_map<int,int> hash;
        hash[0] = 1;
        int ret = 0 , sum = 0;
        for(auto e : nums)
        {
            sum += e ;
            int r = (sum % k + k) %k ;
            if(hash.count(r)) ret += hash[r];
            cout << ret << " " << r << endl;
            hash[r] ++;
        }
        return ret;
    }
};