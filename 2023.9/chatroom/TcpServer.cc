
#include "TcpServer.hpp"

using namespace chatroom;

int main(int argc , char* argv[])
{
    if(argc != 2)
    {
        std::cout << "Usage :  " << argv[0] << "  serverport" << std::endl;
        exit(USAG_ERR);
    }
    //提取端口
    uint16_t port = atoi(argv[1]);
    //创建用户管理系统
    UserManage* um = new UserManage();
    //创建频道管理系统
    ManageChannel* mc = new ManageChannel();
    chatroom::TcpServer tcpserver(um,mc,port); 
    tcpserver.init();
    tcpserver.start();

    return 0 ;
}