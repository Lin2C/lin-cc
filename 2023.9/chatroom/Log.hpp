#pragma once 
#include <iostream>
#include <string>
#include <time.h>
#include <stdarg.h>

#define NORMAL_LOG "nomal_log.txt"
#define ERROR_LOG "error_log.txt"

#define DEBUG 0
#define NORMAL 1
#define WARING 2
#define ERROR 3
#define FATAL 4

const char* LevelMap[]
{
    "DEBUG",
    "NORMAL",
    "WARING",
    "ERROR",
    "FATAL"
};

#define logMessage(level,format,...) _logMessage(level,__LINE__,__FILE__,format,##__VA_ARGS__) 

void _logMessage(int level,int line,const char* filename,const char* format...)
{
    char stdbuff[1024] = {0}; //提取前半部分的缓冲区
    time_t stamp = time(nullptr); //获取当前时间戳
    struct tm* ltime = localtime(&stamp); //将时间戳转换为ltime的结构体
    int year = ltime->tm_year + 1900;
    int mon = ltime->tm_mon + 1; 
    int day = ltime->tm_mday; 
    int hour = ltime->tm_hour;
    int min = ltime->tm_min;
    int sec = ltime->tm_sec;
    snprintf(stdbuff,sizeof stdbuff,"[%s][%d-%d-%d %d:%d:%d][%s][%d]",LevelMap[level],year,mon,day,hour,min,sec,filename,line);

    char argsbuff[1024] = {0}; //提取用户自定义消息的缓冲区
    va_list args;
    va_start(args,format); 
    vsnprintf(argsbuff,sizeof argsbuff,format,args); 
    va_end(args);
    fprintf(stdout,"%s : %s\n",stdbuff,argsbuff); //stdout往显示器上打印，也可以往文件中写入
    // FILE* n_log = fopen(NORMAL_LOG,"a");
    // FILE* e_log = fopen(ERROR_LOG,"a");
    // if(n_log != nullptr && e_log != nullptr)
    // {
    //     FILE* curr = nullptr;
    //     if(level == DEBUG || level == NORMAL|| level == WARING) curr = n_log;
    //     if(level == ERROR || level == FATAL)  curr = e_log;
    //     if(curr) fprintf(curr,"%s : %s\n",stdbuff,argsbuff); //stdout往显示器上打印，也可以往文件中写入
    //     fclose(n_log);
    //     fclose(e_log);
    // }
}