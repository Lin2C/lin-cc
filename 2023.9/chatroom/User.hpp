#pragma once 
#include<string>
#include <unordered_map> 
#include "channel.hpp"
#include <pthread.h>
namespace chatroom
{
    class User
    {
    public:
        User():_is_join(false),_rid(-1){}
        User(int uid ,const std::string& name,int sock) : _uid(uid) , _name(name) ,_is_join(false)
        ,_rid(-1),_sock(sock){}
        //用户行为... 待定
        // bool JoinChannel(Channel* channel) 
        // {
        //     if(channel == nullptr) return false;
        //     _channel = channel; 
        //     return true;
        // }
        //退出频道
        bool QuitChannel();
        std::string& GetName(){return _name;}
        int GetId(){return _uid;}
        int GetSock(){return _sock;}
        bool IsJoin(){return _is_join;}
        void SetIsJoin(bool falg){_is_join = falg;}
        void SetRid(int rid){_rid = rid;}
        int GetRid(){return _rid;}
        void SetName(const std::string& name){_name = name;}
        void SetSock(int sock) { _sock = sock;}
        void SetUid(int uid) {_uid = uid;}
    private:
        int _uid;  //用户id
        std::string _name;  //用户姓名
        bool _is_join; //用户所在频道
        int _sock;  //用户套接字
        int _rid; //所在的房间id，-1为未进入房间
    };


    //用户管理模块
    class UserManage
    {
    private:
        std::unordered_map<int,User*> _users;  
        int _idx; //用户id索引
        pthread_mutex_t _mtx;
    public:
        UserManage():_idx(10000){
            pthread_mutex_init(&_mtx,nullptr);
        }
        ~UserManage()
        {
            pthread_mutex_destroy(&_mtx);
        }
        //查找用户是否上线 
        bool FindUser(int uid)
        {
            pthread_mutex_lock(&_mtx);
            auto it = _users.find(uid); 
            if(it == _users.end()) 
            {
                //用户不在线
                pthread_mutex_unlock(&_mtx);
                return false;
            }
            pthread_mutex_unlock(&_mtx);
            return true;
        }
        //用户上线
        bool AddUser(const std::string& name,User* us,int sock)
        {
            logMessage(DEBUG,"添加用户，sock ：%d",sock);
            pthread_mutex_lock(&_mtx);
            logMessage(DEBUG,"添加用户，us->GetSock() ：%d",us->GetSock());
            if(us == nullptr)
            {
                pthread_mutex_unlock(&_mtx);
                logMessage(ERROR,"adding User failed"); 
                return false; 
            }
            _users.insert({_idx,us}); 
            _idx ++ ;
            pthread_mutex_unlock(&_mtx);
            logMessage(DEBUG,"添加用户，us->GetSock() ：%d",us->GetSock());
            return true;
        }
        //用户下线
        bool RemoveUser(int uid)
        {
            auto it = _users.find(uid) ;
            pthread_mutex_lock(&_mtx);
            if(it == _users.end())
            {
                pthread_mutex_unlock(&_mtx);
                return false; 
            }
            _users.erase(it);
            pthread_mutex_unlock(&_mtx);
            return true;
        }

        int GetIdx(){return _idx;}

    };

    
};