#include<stdio.h>
#include<stdlib.h>
#include<signal.h>
#include<unistd.h>
#include<string.h>


void handler(int signo)
{
  printf("signal : %d\n",signo);
  exit(1);
}

//struct sigaction测试
int main()
{
  struct sigaction act;
  memset(&act,0,sizeof(act));
  
  act.sa_handler = handler;//把函数的地址放进的handler表中
  sigemptyset(&act.sa_mask);
  sigaddset(&act.sa_mask,2);
  act.sa_handler = SIG_IGN;
  sigaction(2,&act,NULL);
  while(1)
  {
    printf("hello world\n");
    sleep(1);
  }


  return 0;
}



//sigset 位图测试 
//int main()
//{
//  signal(2,handler);
//  sigset_t iset,oset;
//  sigemptyset(&iset); //位图初始化
//  sigemptyset(&oset);
//  sigaddset(&iset,2); //位图注入2号信号
//  sigprocmask(SIG_SETMASK, &iset, &oset); //自定义处理信号,并把Block表替换
//
//  int count = 0;
//  while(1)
//  {
//    count ++;
//    printf("Hello world\n");
//    sleep(1);
//    if(count == 5)
//    {
//      //解除信号阻塞 
//      sigprocmask(SIG_SETMASK,&oset,NULL);
//    }
//  }
//
//
//
//  return 0;
//}
