
#include<pthread.h>
#include<time.h>
#include<cstdlib>
#include "ring_queue.hpp"
#include<unistd.h>

using namespace nm_ringqueue;



void* producer(void* args)
{
    ringqueue<int>* rq =(ringqueue<int>*)args; 
    while(true)
    {
        int data = rand()%20+1;    
        std::cout<<"生产数据 ： "<<data<<std::endl;
        //printf("生产者生产的数据是 ： %d\n",data);   
         rq->push(data); //生产data
        sleep(1);
    }
}

void* consumer(void* args)
{
    ringqueue<int>* rq =(ringqueue<int>*)args; 

    while(true)
    {
        int data = 0;
        rq->pop(&data);
        std::cout<<"消费数据 ： "<<data<<std::endl;
       //printf("消费者拿到的数据是 ： %d\n",data);
    }

}



int main()
{
    srand((long long)time(nullptr));
    pthread_t c,p;
    ringqueue<int>* rq = new ringqueue<int>(); 
    pthread_create(&c,nullptr,consumer,(void*)rq);
    pthread_create(&p,nullptr,producer,(void*)rq);



    pthread_join(c,nullptr);
    pthread_join(p,nullptr);

    return 0;
}
