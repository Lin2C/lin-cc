
#include<unistd.h>
#include<cstdlib>
#include<time.h>
#include "BlockQueue.hpp"

using namespace ns_blockqueue;

//消费者
void* consumer(void* args)
{
     blockqueue<int> *bq = (blockqueue<int>*)args;
     while(true)
     {
        //不断的拿数据
        int data;
        bq->pop(&data);
        std::cout<<"消费者消费了一个数据 : "<<data<<std::endl;
        sleep(1);
     }

}

//生产者
void* producer(void* args)
{
     blockqueue<int> *bq = (blockqueue<int>*)args;
     //生产数据
     while(true)
     {
        int data = rand() % 20 + 1;
        bq->push(data);
        std::cout<<"生产者生产了一个数据 : "<<data<<std::endl;
     }
}


int main()
{
    srand((long long)time(nullptr));
    blockqueue<int> *bq = new blockqueue<int>();
    //创建消费者进程和生产者进程
    pthread_t c,p;
    pthread_create(&c,nullptr,consumer,(void*)bq);
    pthread_create(&p,nullptr,producer,(void*)bq);


    //等待两个线程
    pthread_join(c,nullptr);
    pthread_join(p,nullptr);
    
    return 0;
}
