#include"BlockQueue.hpp"
#include<time.h>
#include<cstdlib>
#include<unistd.h>

using namespace ns_BlockQueue;

void* producer(void* args)
{
    BlockQueue<int>*bq = (BlockQueue<int>*)args;
    //不断生产数据
    while(true)
    {
        int data = rand()%20+1;
        std::cout<<"生产者生产 -> "<<data<<std::endl;
        bq->push(data);
    }
}   
void* consumer(void* args)
{
    BlockQueue<int>*bq = (BlockQueue<int>*)args;
    while(true)
    {
        int data = 0;
        bq->pop(&data);
        std::cout<<"消费消费 -> "<<data<<std::endl;
        sleep(1);
    }

}

int main()
{
    srand((long long)time(nullptr));
    BlockQueue<int>* bq = new BlockQueue<int>();
    //创建两个线程
    pthread_t c,p;
    pthread_create(&c,nullptr,consumer,(void*)bq);
    pthread_create(&p,nullptr,producer,(void*)bq);

    //main线程等待2个线程
    pthread_join(c,nullptr);
    pthread_join(p,nullptr);

}