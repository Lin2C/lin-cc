#include"ring_queue.hpp"
#include<time.h>
#include<cstdlib>
#include<unistd.h>
#include"Task.hpp"
using namespace ns_ring_queue;
using namespace ns_Task;


void* consumer(void* args)
{
    ring_queue<Task>* rq = (ring_queue<Task>*)args;
    while(true)
    {
        Task t;
        rq->Pop(&t);
      //  std::cout<<"消费了一个数据" << data<<std::endl;
        t();
        sleep(1);
    }
}
void* producer(void* args)
{
    ring_queue<Task>* rq = (ring_queue<Task>*)args;
    const std::string op = "+-*/%";
    while(true)
    {
        int x = rand() %20+1;
        int y = rand() % 10 +1 ;
        Task t(x,y,op[rand()%5]);
        //std::cout<<"生产的任务是 ： " << "我是: " << pthread_self()<< t.show()<<std::endl;
        printf("生产的任务是: %s，我是%lu\n",t.show().c_str(),pthread_self());
        rq->Push(t);
    }
}


int main()
{
    srand((long long)time(nullptr));
    ring_queue<Task>* rq = new  ring_queue<Task>();
    pthread_t c1,c2,c3,c4,p1,p2,p3,p4;
    pthread_create(&c1,nullptr,consumer,(void*)rq);
    pthread_create(&c2,nullptr,consumer,(void*)rq);
    pthread_create(&c3,nullptr,consumer,(void*)rq);
    pthread_create(&c4,nullptr,consumer,(void*)rq);

    pthread_create(&p1,nullptr,producer,(void*)rq);
    pthread_create(&p2,nullptr,producer,(void*)rq);
    pthread_create(&p3,nullptr,producer,(void*)rq);
    pthread_create(&p4,nullptr,producer,(void*)rq);


    pthread_join(c1,nullptr);
    pthread_join(c2,nullptr);
    pthread_join(c3,nullptr);
    pthread_join(c4,nullptr);

    pthread_join(p1,nullptr);
    pthread_join(p2,nullptr);
    pthread_join(p3,nullptr);
    pthread_join(p4,nullptr);

    return 0;
}