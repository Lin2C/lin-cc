#pragma once
#include<iostream>
#include<pthread.h>


namespace ns_Task
{

    class Task
    {
    private:
        int _x;
        int _y;
        char _op;
    
    private:
        int Run()
        {
            int res = 0;
            switch(_op)
            {
                case '+':
                    res = _x+_y;
                    break;
                case '-':
                    res = _x-_y;
                    break;
                case '*':
                    res = _x*_y;
                case '/':
                    res = _x/_y;
                        break;
                case '%':
                    res = _x % _y;
                    break;        
                default :
                    std::cout<<"bug?"<<std::endl;
                    break;
            }
            //std::cout<<"当前任务正在被 " <<pthread_self()<<" 处理 : "<<
            //_x << _op << _y << "=" << res<<std::endl;
            printf("当前任务正在被 %lu 处理，%d%c%d=%d\n",pthread_self(),_x,_op,_y,res);
            return res;
        }   
    public:
        std::string show()
        {
            std::string message;
            message += std::to_string(_x);
            message += _op;
            message += std::to_string(_y);
            message += "=?";
            return message;
        }
        Task(){}
        Task(int x,int y,int op)
        :_x(x),_y(y),_op(op){}
        
        //仿函数
        int operator()()
        {
             return Run(); 
        }
    
    };
}
