#define _CRT_SECURE_NO_WARNINGS 1
// SeqList.h
#include"SeqL.h"



// 对数据的管理:增删查改 
void SeqListInit(SeqList* ps)
{
	ps->capacity = ps->size = 0;
	ps->a = NULL;
}

void SeqListDestroy(SeqList* ps)
{
	ps->capacity = ps->size = 0;
	free(ps->a);
}

void SeqListPrint(SeqList* ps)
{
	for (int i = 0; i < ps->size; i++)
	{
		printf("%d ", ps->a[i]);
	}
	printf("\n");
}

void SeqListPushBack(SeqList* ps, SLDateType x)
{
	SeqListInsert(ps, ps->size, x);
}

void SeqListPushFront(SeqList* ps, SLDateType x)
{
	SeqListInsert(ps,0 , x);
}
void SeqListPopFront(SeqList* ps)
{
	SeqListErase(ps, 0);
}
void SeqListPopBack(SeqList* ps)
{
	SeqListErase(ps, ps->size - 1);
}

// 顺序表查找
int SeqListFind(SeqList* ps, SLDateType x)
{
	for (int i = 0; i < ps->size; i++)
	{
		if (x == ps->a[i])
			return i;
	}
	return -1;
}

void addcacpcity(SeqList* ps,size_t size)
{
	SLDateType* newA = realloc(ps->a, size * sizeof(SLDateType));
	if (newA == NULL)
	{
		perror("realloc");
		exit(1);
	}
	ps->a = newA;
	ps->capacity = size;
}

// 顺序表在pos位置插入x
void SeqListInsert(SeqList* ps, int pos, SLDateType x)
{
	if (ps->size == ps->capacity)
	{
		//扩容
		addcacpcity(ps, ps->capacity == 0 ? 8 : ps->capacity * 2);
	}
	for (int i = ps->size - 1 ; i >= pos; i--)
	{
		(ps->a)[i + 1] = (ps->a)[i];
	}
	(ps->a)[pos] = x;
	ps->size++;
}
// 顺序表删除pos位置的值
void SeqListErase(SeqList* ps, int pos)
{
	assert(pos < ps->size);
	int begin = pos + 1;
	while (begin < ps->size)
	{
		(ps->a)[begin - 1] = (ps->a[begin]);
		begin++;
	}
	ps->size--;
	
}


//test
int main()
{
	SeqList sq;
	SeqListInit(&sq);
	//SeqListInsert(&sq, 0,1);
	//SeqListInsert(&sq, 0, 2);
	//SeqListInsert(&sq, 0, 3);
	//SeqListInsert(&sq, 0, 4);
	//SeqListPrint(&sq);
	//SeqListErase(&sq, 0);
	//SeqListPrint(&sq);
	//SeqListErase(&sq, 0);
	//SeqListPrint(&sq);
	//SeqListErase(&sq, 0);
	//SeqListPrint(&sq);
	//SeqListErase(&sq, 0);
	//SeqListPrint(&sq);
	//SeqListErase(&sq, 0);

	SeqListPushBack(&sq, 1);
	SeqListPushBack(&sq, 2);
	SeqListPushBack(&sq, 3);
	SeqListPrint(&sq);
	SeqListPopBack(&sq);
	SeqListPrint(&sq);
	SeqListPopBack(&sq);
	SeqListPrint(&sq);
	SeqListPopBack(&sq);

	SeqListDestroy(&sq);
	return 0;
}