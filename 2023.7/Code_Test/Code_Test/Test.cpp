#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
#include<algorithm>
#include<vector>
#include<string>
using namespace std;
//
//void for_each_test(int i)
//{
//	cout << i << endl;
//}
//
//
//int main()
//{
//	//vector<int> f = { 1.1,2,3,4,5 };
//	for_each(f.begin(), f.end(), for_each_test);
//	return 0;
//}


//class A
//{
//public:
//	int n = 100;
//private:
//	void fun()  // const A*  -> n
//	{
//		n = 1000; 
//	}
//};

/*

mutable关键字的使用
以及对const对象和非const对象操作符重载的复用

class CTextBlock
{
public:
	CTextBlock() {}
	CTextBlock(const string& s)
	{
		Ptext = s;
	}
	std::size_t Length() const
	{
		if (!IsValid)
		{
			len = Ptext.size();
			IsValid = true;
		}
		return len;
	}

	char& operator[](std::size_t i)
	{
		return const_cast<char&>(
			static_cast<const CTextBlock>(*this)[i]
			);
	}

	const char& operator[](std::size_t i) const
	{
		return Ptext[i];
	}
	
	void print()
	{
		cout << Ptext << endl;
	}
private:
	std::string Ptext;
	mutable std::size_t len;
	mutable bool IsValid;  //mutable的作用就是让常量对象的值可在内部被修改
};

int main()
{
	CTextBlock cb("hellowolrd");
	cb[0] = 'J';
	cb.print();
	return 0;
}

*/

/* 如果一个对象含有引用成员变量，那么默认的 operator=()函数会被编译器自动删除 
template<class T> 
class dog
{
public:
	dog(){}
	dog(const T _age , string& _name) : age(_age),name(_name){}
private:
	const T age; 
	string& name;
};

int main()
{
	string name1("大黄");
	string name2("小黑");

	dog<int> d1(16, name1);
	dog<int> d2(88,name2);
	d1 = d2;

	return 0;
}
*/

/*如果基类虚函数没有virtual ， 那么释放基类时，派生类不会自动调用析构。反之先调用
派生类的析构，再调用基类的析构 
class A
{
public:
	A(){}
	A(const string& s):_s(s){}
	virtual ~A(){
		cout << "A资源被释放" << endl;
	}
private:
	string _s;
};

class B : public A
{
public:
	B() {}
	B(const string& s) :_s(s) {}
	 ~B(){
		cout << "B资源被释放" << endl;
	}
private:
	string _s;
};


B* test()
{
	A* a;
	B* b = new B("abcde");
	a = b;
	delete a;
	return b;
}

int main()
{
	B* b = test();
	
	return 0;
}
*/
//
//class father
//{
//public:
//	virtual void log() const = 0;
//	father()
//	{
//		log();
//	}
//};
//
//class son1 : public father
//{
//	virtual void log() const
//	{
//		std::cout << "l0g" << std::endl;
//	}
//};

//
//int main()
//{
//	vector<bool> v = { false,true,false ,false ,false ,false ,false ,false ,false ,false ,false };
//	bool a = true;
//	int b;
//	cout << sizeof(bool) << endl;
//	const bool& tmp = v.at(0);
////	bool* p = &v[0];
//
//	return 0;
//}

//int main()
//{
//	int  i = 6, arr[5] = { 1 , 2, 3 ,4 ,5 };
//	cout << arr[7] << endl;
//
//
//	return 0;
//}


//
//template<class T>
//void Swap(T& x, T& y)
//{
//	T tmp = x;
//	x = y; 
//	y = tmp;
//}
//
//int main()
//{
//	int i1 = 5, i2 = 10;
//	double d1 = 5.5, d2 = 10.5;
//	string str1 = "hello", str2 = "world";
//	cout << "交换前：" << endl;
//	cout << "i1 : " << i1 << " , i2 :" << i2 << endl;
//	cout << "d1 : " << d1 << " , d2 :" << d2 << endl;
//	cout << "str1 : " << str1 << " , str2 :" << str2 << endl;
//	
//	Swap(i1, i2);
//	Swap(d1, d2);
//	Swap(str1, str2);
//	cout << "交换后：" << endl;
//	cout << "i1 : " << i1 << " , i2 :" << i2 << endl;
//	cout << "d1 : " << d1 << " , d2 :" << d2 << endl;
//	cout << "str1 : " << str1 << " , str2 :" << str2 << endl;
//
//	return 0;
//}

//template <class T>
//class Node
//{
//public:
//	void fun()
//	{
//		cout << "T* : " << typeid(next).name() << endl;
//		cout << "T : " << typeid(val).name() << endl;
//	}
//private:
//	T* next;
//	T val;
//};
//
//int main()
//{
//	Node<int> ni;
//	ni.fun();
//	cout << endl;
//
//	Node<double> nd;
//	nd.fun();
//	cout << endl;
//
//	Node<char> nc;
//	nc.fun();
//
//	return 0;
//}


////普通模板
//template<class T>
//class comp
//{
//public:
//	comp(T a) :_a(a)
//	{
//		cout << "comp()" << endl;
//	}
//	bool operator<(const comp<T>& c)
//	{
//		return _a < c._a;
//	}
//private:
//	T _a;
//};
//
////特化模板
//template<>
//class comp<int*>
//{
//public:
//	comp(T a) :_a(a)
//	{
//		cout << "comp()" << endl;
//	}
//	bool operator<(const comp<T>& c)
//	{
//		return _a < c._a;
//	}
//private:
//	T _a;
//};
//
//int main()
//{
//	int a = 5, b = 10;
//	comp<int*> c1(&a); 
//	comp<int*> c2(&b);
//
//	if (c1 < c2)
//	{
//		cout << "c1 < c2" << endl;
//	}
//	else
//	{
//		cout << "c1 >= c2" << endl;
//	}
//}

//
//
//template<class T1,class T2>
//class A
//{
//public:
//	A()
//	{
//		cout << "A<T1,T2>" << endl; 
//	}
//};
//
//template<class T1>
//class A<T1,char>
//{
//public:
//	A()
//	{
//		cout << "A<T,char>" << endl;
//	}
//};
//
//int main()
//{
//	A<int,int> a1;
//	A<double,char> a2;
//	A<char,char>a3;
//
//	return 0;
//}-


//class A
//{
//public:
//	int _a;
//};
//
//class B : virtual public A
//{
//public:
//	int _b;
//};
//
//class C :virtual public A
//{
//public:
//	int _c;
//};
//
//class D : public B, public C
//{
//public:
//	int _d;
//};
//
//int main()
//{
//	D d;
//	d._b = 2; 
//	d._c = 3;
//	d._d = 4;
//	d.B::_a = 5;
//	d.C::_a = 6;
//}
//
//class A
//{
//public:
//	void a()
//	{
//		cout << "a" << endl;
//	}
//	void b()
//	{
//		cout << "b" << endl;
//	}
//	int _a = 5;
//};
//
//int main()
//{
//	A a;
//	cout << &A::a << endl;
//	cout << &A::b << endl;
//
//}


//class A
//{
//private:
//	static void fun()
//	{
//		cout << 1 << endl;
//	}
//public:
//	static int a;
//};
//
//int main()
//{
//	A a;
//	a.a;
//	return 0;
//}

const int N = 20;
int f[N][N];
//
//int main() {
//	int n, m;
//	cin >> n >> m;
//	for (int i = 1; i <= n+1; i++) f[i][1] = 1;
//	for (int j = 1; j <= m+1; j++) f[1][j] = 1;
//	for (int i = 2; i <= n + 1; i++)
//	{
//		for (int j = 2; j <= m + 1; j++)
//		{
//			f[i][j] = f[i - 1][j] + f[i][j - 1];
//		}
//	}
//	cout << f[n + 1][m + 1];
//	return f[n+1][m+1];
//}


int main()
{
	for (int i = 0; i < 50; i++)
	{
		char name[50] = { 0 };
		int a = 1000;
		printf("%p,%p\n", name,&a);
	}
	return 0;
}