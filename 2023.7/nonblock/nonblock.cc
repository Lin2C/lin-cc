#include<iostream>
#include<fcntl.h>
#include<unistd.h>
#include<cstring>

using namespace std;

bool SetNonBlock(int fd)
{
    int f1 = fcntl(fd,F_GETFL); //获取fd对应的文件读写的标志位
    if(f1 < 0)
        return false;
    
    fcntl(fd,F_SETFL,f1 | O_NONBLOCK ); //设置非阻塞等待
}

int main()
{
    SetNonBlock(0); //设置标准输入非阻塞等待

    char buff[1024] = {0};
    while(true)
    {
        sleep(1);
        errno = 0 ; 
        memset(buff,0,sizeof buff);
        int s = read(0,buff,sizeof buff-1);
        buff[s-1] = 0;  //去掉空行

        if(s > 0 )
        {
            //读取成功
            cout << buff << "  数据读取成功 ->  errno : " << errno << endl; 
        }else
        {
            if(errno == EWOULDBLOCK || errno == EAGAIN)
            {
                //没有读取到数据
                 cout << "数据正在等待读取 ...." << "   errno : " << errno << endl; 
                 continue;
            }else if(errno == EINTR)
            {
                //等待中收到信号
                 cout << "IO被信号中断... " << "   errno : " << errno << endl; 
                 continue;
            }
            else
            {
                //读取出错.... 
                break;
            }
        }
    }
}
