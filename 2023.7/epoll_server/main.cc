#include<memory>
#include "EpollServer.hpp"

void handler_request(std::string str)
{
    std::cout << "message : " << str ;
}

int main()
{
    std::unique_ptr<wyl::EpollServer> svr(new wyl::EpollServer(handler_request));
    svr->Start();
    return 0;
}