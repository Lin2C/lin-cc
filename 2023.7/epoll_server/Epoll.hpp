#pragma ocne 

#include<iostream>
#include<sys/epoll.h>
#include "Log.hpp"

namespace wyl
{
    class Epoll
    {
    public:
        static int CreateEpoll()
        {
            int n = epoll_create(256); //废弃的参数
            if(n < 0 )
            {
                DLOG("create epoll failed\n");
                exit(2);
            } 
            return n;
        }
        static bool EpollCtl(int epfd,int oper,int sock,uint32_t event)
        {
            struct epoll_event ev; 
            ev.data.fd = sock; 
            ev.events = event; 
            int n = epoll_ctl(epfd,oper,sock,&ev);
            return n == 0;
        }
        static int EpollWait(int epfd , struct epoll_event events[],int maxevents,int timeout)
        {
            return epoll_wait(epfd,events,maxevents,timeout);
        }
    private:
    };
}