    int maxTurbulenceSize(vector<int>& arr) {
        int n = arr.size();
        vector<vector<int>> f(n + 1, vector<int>(2,1)); //f[i][0]为下降，f[i][1]为上升
        int ret = 1;
        for(int i = 2 ; i <= n; i ++)
        {
            if(arr[i-1] < arr[i-2])
            {
                //下降趋势，更新f[i][0]
                f[i][0] = f[i-1][1] + 1; //从f[i-1][1]上升趋势中更新下降趋势
            }else if(arr[i-1] > arr[i-2])
            {
                //上升趋势，更新f[i][1]
                f[i][1] = f[i-1][0] + 1; //从f[i-1][0]下降趋势中更新上升趋势
            }
            ret = max(ret,max(f[i][1],f[i][0]));
        }
        return ret;
    }