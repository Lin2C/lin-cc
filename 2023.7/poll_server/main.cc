#include "poll_server.hpp"
#include<memory>

int main()
{
    std::unique_ptr<PollServer> sv(new PollServer());
    sv->Start();
    return 0;
}