#pragma once

#include <iostream>
#include <string>
#include <cstring>
#include <vector>
#define SEP "X"

#define SPACE " "
#define SPACE_LEN strlen(SPACE)

namespace wyl
{

    void SpliteMessage(std::string &buffer, std::vector<std::string> *out)
    {
        while (true)
        {
            int pos = buffer.find(SEP);
            if (std::string::npos == pos)
                break;
            out->push_back(buffer.substr(0, pos));
            buffer.erase(0, pos + strlen(SEP));
        }
    }

    std::string Encode(std::string &s)
    {
        return s + SEP;
    }

    class Request
    {
    public:
        std::string Serialize()
        {
            std::string str;
            str = std::to_string(x_);
            str += SPACE;
            str += op_; // TODO
            str += SPACE;
            str += std::to_string(y_);
            return str;
        }
        bool Deserialized(const std::string &str) // 1 + 1
        {
            std::size_t left = str.find(SPACE);
            if (left == std::string::npos)
                return false;
            std::size_t right = str.rfind(SPACE);
            if (right == std::string::npos)
                return false;
            x_ = atoi(str.substr(0, left).c_str());
            y_ = atoi(str.substr(right + SPACE_LEN).c_str());
            if (left + SPACE_LEN > str.size())
                return false;
            else
                op_ = str[left + SPACE_LEN];
            return true;
        }

    public:
        Request()
        {
        }
        Request(int x, int y, char op) : x_(x), y_(y), op_(op)
        {
        }
        ~Request() {}

    public:
        int x_;   // 是什么？
        int y_;   // 是什么？
        char op_; // '+' '-' '*' '/' '%'
    };

    class Response
    {
    public:
        // "code_ result_"
        std::string Serialize()
        {

            std::string s;
            s = std::to_string(code_);
            s += SPACE;
            s += std::to_string(result_);

            return s;
        }
        // "111 100"
        bool Deserialized(const std::string &s)
        {
            std::size_t pos = s.find(SPACE);
            if (pos == std::string::npos)
                return false;
            code_ = atoi(s.substr(0, pos).c_str());
            result_ = atoi(s.substr(pos + SPACE_LEN).c_str());
            return true;
        }

    public:
        Response()
        {
        }
        Response(int result, int code) : result_(result), code_(code)
        {
        }
        ~Response() {}

    public:
        // 约定！
        // result_? code_? code_ 0? 1?2?3?
        int result_; // 计算结果
        int code_;   // 计算结果的状态码
    };

}