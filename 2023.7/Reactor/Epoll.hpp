#pragma once 

#include<iostream>
#include <cstring>
#include<sys/epoll.h>
#include "Log.hpp"

namespace wyl
{
    const static int gnum = 128;
    const static int TIMEOUT = 5000;
    class Epoll
    {
    public:
        Epoll(int timeout = TIMEOUT) : _timeout(timeout)
        {
        }

        void CreateEoll()
        {
            _epfd = epoll_create(gnum);
            if(_epfd < 0 )
            {
                ELOG("epoll create failed\n"); 
                exit(5);
            }
            DLOG("epoll create success\n"); 
        }

        //删除
        bool DelSockFromEpoll(int sock,uint32_t event)
        {
           return CtlEpoll(EPOLL_CTL_DEL,sock,event);
        }
        //修改
        bool ModSockFromEpoll(int sock,uint32_t event)
        {
           return CtlEpoll(EPOLL_CTL_MOD,sock,event);
        }
        //添加
        bool AddSockToEpoll(int sock,uint32_t event)
        {
            return CtlEpoll(EPOLL_CTL_ADD,sock,event);
        }

        int WaitEpoll(epoll_event* revs,int revs_num)
        {
            return epoll_wait(_epfd,revs,revs_num,_timeout);
        }

        ~Epoll()
        {

        }
    private: 
         bool CtlEpoll(int op , int sock,uint32_t event)
         {
            struct epoll_event ev ;
            ev.data.fd = sock;
            ev.events = event; 
            int ret = epoll_ctl(_epfd,op,sock,&ev); 
            return ret == 0;
         }
    private:
        int _epfd; 
        int _timeout;
    };
};