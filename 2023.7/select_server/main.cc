#include"2_select_server.hpp"
#include<memory>

int main()
{
    std::unique_ptr<SelectServer> sv(new SelectServer());
    sv->Start();
    return 0;
}