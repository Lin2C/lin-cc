#pragma once 

#include<sys/types.h>
#include<stdio.h>
#include<sys/fcntl.h> 
#include<unistd.h> 
#include<string.h>
#include<assert.h>
#include<stdlib.h>

#define SIZE 1024 
#define SYNC_NOW 1
#define SYNC_LINE 2
#define SYNC_FULL 4

typedef struct _FILE{
  int flag; 
  int fd;  
  char buff[SIZE];
  size_t cap; 
  size_t size; 

}FILE_; 


FILE_*  fopen_(const char* path_name,const char* mod);

void fwrite_(FILE_* file,const void* buff,size_t num);
void fflash_(FILE_* file);
void fclose_(FILE_* file);


