#pragma once
#include<vector>
using namespace std;

namespace wyl
{
	template<size_t N>
	class bit_set
	{
	public:
		bit_set()
		{
			_bt.resize(N / 8 + 1);
		}

		void set(const size_t& key)
		{
			size_t i = key / 8;
			size_t j = key % 8;
			_bt[i] |= 1 << j;
		}

		void reset(const size_t& key)
		{
			size_t i = key / 8;
			size_t j = key % 8;
			_bt[i] &= ~(1 << j);
		}

		bool test(const size_t& key)
		{
			size_t i = key / 8;
			size_t j = key % 8;
			return _bt[i] & (1 << j);
		}
	private:
		vector<char> _bt;
	};

}