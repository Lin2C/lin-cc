#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
#include<algorithm>
using namespace std;

const int N = 110;
int f[N][N];
int w[N][N];
int n, m;

int main()
{
    int T;
    cin >> T;
    while (T--)
    {
        cin >> n >> m;
        for (int i = 1; i <= n; i++)
            for (int j = 1; j <= m; j++)
                scanf("%d", &w[i][j]);
        f[1][1] = w[1][1];
        for (int i = 1; i <= n; i++)
            for (int j = 1; j <= m; j++)
                f[i][j] = max(f[i - 1][j] + w[i][j], f[i][j - 1] + w[i][j]);
        cout << f[n][m] << endl;
    }
    return 0;
}