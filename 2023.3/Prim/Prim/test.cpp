#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
#include<cstring>
#include<algorithm>
using namespace std;

const int N = 510, INF = 0x3f3f3f3f, M = 10010;
int g[N][N], dist[N];
bool st[M];
int n, m;


int prim()
{
    int res = 0;//记录所有边的权重之和
    memset(dist, 0x3f, sizeof dist);
    for (int i = 0; i < n; i++)
    {
        int t = -1; //存在在集合中的最短边
        for (int j = 1; j <= n; j++)
            if (!st[j] && (t == -1 || dist[t] > dist[j]))
                t = j;
        if (i && dist[t] == INF) return INF; //这条边没有连通，不存在最小生成树
        st[t] = true;
        if (i) res += dist[t]; //加上该边的权重，第一条边不加
        for (int j = 1; j <= n; j++)
            dist[j] = min(dist[j], g[t][j]); // 注意和dijkstra的区别
                                            // prim的dist是到已经生成的树(集合)的最短距离
    }
    return res;
}

int main()
{
    scanf("%d%d", &n, &m);
    memset(g, 0x3f, sizeof g);
    while (m--)
    {
        int a, b, c;
        scanf("%d%d%d", &a, &b, &c);
        g[a][b] = g[b][a] = min(g[a][b], c);
    }
    int t = prim();
    if (t == INF) puts("impossible");
    else printf("%d\n", t);

    return 0;
}