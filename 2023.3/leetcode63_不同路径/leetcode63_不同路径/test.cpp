#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
#include<vector>
using namespace std;

int uniquePathsWithObstacles(vector<vector<int>>& obstacleGrid) {
    int f[110][110];
    int n = obstacleGrid.size(), m = obstacleGrid[0].size();
    memset(f, 0, sizeof f);
    for (int i = 1; i <= n; i++) { if (!obstacleGrid[i - 1][0]) f[i][1] = 1;  else break; }
    for (int j = 1; j <= m; j++) { if (!obstacleGrid[0][j - 1]) f[1][j] = 1;  else break; }
    f[0][1] = 1; //初始化一下最初的边界
    for (int i = 1; i <= n; i++)
        for (int j = 1; j <= m; j++)
            if (!obstacleGrid[i - 1][j - 1]) f[i][j] = f[i - 1][j] + f[i][j - 1];

    return f[n][m];
}

int main()
{
    vector<vector<int>> obstacleGrid = { {0, 0, 0},{0, 1, 0},{0, 0, 0} };
    cout << uniquePathsWithObstacles(obstacleGrid) << endl;;
    return 0;
}