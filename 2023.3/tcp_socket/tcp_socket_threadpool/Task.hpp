#pragma once
#include <iostream>
#include <string>

namespace wyl
{
    class Task
    {
    private:
        int _sock;

    public:
        Task(int sock) : _sock(sock) {}
        Task()
        {
        }

        int Run()
        {
            // render server
            while (true)
            {
                // operator file
                char buffer[1024] = {0};
                ssize_t s = read(_sock, buffer, sizeof(buffer) - 1);
                if (s > 0)
                {
                    buffer[s] = 0;
                    std::cout << "client# " << buffer << std::endl;
                    std::string s = ">>>server<<<, ";
                    s += buffer;
                    write(_sock, s.c_str(), s.size());
                }
                else if (s == 0)
                {
                    // quit
                    std::cout << "client quit....." << std::endl;
                    break;
                }
                else
                {
                    // erro
                    std::cout << "read error" << std::endl;
                    break;
                }
            }
        }

        int operator()()
        {
            return Run();
        }
    };
}