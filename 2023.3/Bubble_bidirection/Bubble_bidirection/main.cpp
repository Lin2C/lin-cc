#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>

void swap(int* x, int* y)
{
    int tmp = *x;
    *x = *y;
    *y = tmp;
}
void Bubble(int* arr,int len)
{
    int begin = 0;
    int end = len - 1;

    while (begin < end)
    {
        for (int i = begin; i < end; i++)
            if (arr[i] > arr[i + 1])
                swap(&arr[i], &arr[i + 1]);
        for (int i = end; i > begin; i--)
            if (arr[i] < arr[i - 1])
                swap(&arr[i], &arr[i - 1]);
        begin++;
        end--;
    }
}
int main()
{
    int arr[] = { 6,5,4,3,2,1,0 };
    Bubble(arr, sizeof(arr) / sizeof(arr[0]));
    return 0;
}
