#include<iostream>
#include<string>
using namespace std;

const int N = 1e6 + 10;
int n, sz;
int h[N];
int hp[N]; //记录h数组对应的每个值是第几次插入，hp[1] = 4，说明h[1]的值是第四次插入
int ph[N]; //记录h数组每一次插入的坐标，第一次插入就在ph[1]的位置记录该值在h中的下标
//ph[hp[1]], hp[1]记录h数组第一个元素是第几次插入的，ph[hp[1]] 可以直接找到第某一次插入元素在h数组的下标

void heap_swap(int a, int b)
{
    swap(ph[hp[a]], ph[hp[b]]);
    swap(hp[a], hp[b]);
    swap(h[a], h[b]);
}
void down(int u)
{
    int t = u;
    if (u * 2 <= sz && h[t] > h[u * 2]) t = u * 2;
    if (u * 2 + 1 <= sz && h[t] > h[u * 2 + 1]) t = u * 2 + 1;
    if (t != u)
    {
        heap_swap(u, t);
        down(t);
    }
}
void up(int u)
{
    while (u / 2 && h[u] < h[u / 2])
    {
        heap_swap(u, u / 2);
        u >>= 1;
    }
}
int main()
{
    int m = 0;
    scanf("%d", &n);
    while (n--)
    {
        int x, k;
        string op;
        cin >> op;
        if (op == "I")
        {
            scanf("%d", &x);
            m++;
            sz++;
            hp[sz] = m, ph[m] = sz;
            h[sz] = x;
            up(sz);
        }
        else if (op == "PM") printf("%d\n", h[1]);
        else if (op == "DM")
        {
            heap_swap(1, sz);
            sz--;
            down(1);
        }
        else if (op == "D")
        {
            scanf("%d", &k);
            k = ph[k]; //找到第k次在堆中的下标
            heap_swap(k, sz);
            sz--;
            up(k);
            down(k);
        }
        else
        {
            scanf("%d%d", &k, &x);
            k = ph[k];//找到第k次插入数据在堆中的下标
            h[k] = x;
            down(k);
            up(k);
        }
    }
    return 0;
}