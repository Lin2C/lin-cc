#define _CRT_SECURE_NO_WARNINGS 1


/*
给定一个 n×m的二维整数数组，用来表示一个迷宫，数组中只包含 0或 1，其中 0
 表示可以走的路，1表示不可通过的墙壁。最初，有一个人位于左上角 (1,1)
 处，已知该人每次可以向上、下、左、右任意一个方向移动一个位置。
请问，该人从左上角移动至右下角 (n,m)处，至少需要移动多少次。

数据保证 (1,1)处和 (n,m)处的数字为 0，且一定至少存在一条通路。

输入格式
第一行包含两个整数 n和 m。

接下来 n行，每行包含 m个整数（0或 1），表示完整的二维数组迷宫。

输出格式
输出一个整数，表示从左上角移动至右下角的最少移动次数。

数据范围
1≤n,m≤100
输入样例：
5 5
0 1 0 0 0
0 1 0 1 0
0 0 0 0 0
0 1 1 1 0
0 0 0 1 0
输出样例：
8
*/

#include<iostream>
#include<queue>
#include<cstring>
using namespace std;

const int  N = 110;
char g[N][N]; //地图
int dist[N][N]; //起点到每一个点的距离
typedef pair<int, int> PII;
int n;

bool bfs(int l1, int r1, int l2, int r2)
{
    if (g[l1][r1] == '#') return false;
    if (l1 == l2 && r1 == r2) return true;
    memset(dist, -1, sizeof dist);
    dist[l1][r1] = 0;
    queue<PII> q;
    q.push({ l1,r1 });
    int dx[4] = { 1,0,-1,0 }, dy[4] = { 0,1,0,-1 };
    while (q.size())
    {
        PII t = q.front();
        q.pop();
        for (int i = 0; i < 4; i++)
        {
            int x = t.first + dx[i], y = t.second + dy[i];
            if (x >= 0 && x < n && y >= 0 && y < n && dist[x][y] == -1 && g[x][y] == '.')
            {
                q.push({ x,y });
                dist[x][y] = dist[t.first][t.second] + 1;
                if (x == l2 && y == r2) return true;
            }
        }
    }
    return false;
}

int main()
{
    int k = 0;
    scanf("%d", &k);
    while (k--)
    {
        memset(g, '#', sizeof g);
        int l1, r1, l2, r2;
        scanf("%d", &n);
        for (int i = 0; i < n; i++) scanf("%s", &g[i]);

        scanf("%d%d%d%d", &l1, &r1, &l2, &r2);
        if (bfs(l1, r1, l2, r2)) printf("YES\n");
        else printf("NO\n");
    }

    return 0;
}

