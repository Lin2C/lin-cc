#include"Thread_pool.hpp"
#include"Task.hpp"
#include<iostream>
#include<time.h>
#include<cstdlib>
#include<unistd.h>

using namespace wyl;

int main()
{
    srand((long long)time(nullptr));
    char ops[] = "+-*/%";
    //创建线程池
    Thread_pool<Task>* tp = new  Thread_pool<Task>(5);
    tp->Pool_Init();
    while(true)
    {
        Task t(rand()%20+1,rand()%10+1,ops[rand()%5]);
        tp->Push(t);
        sleep(1);
    }

    return 0;    
}