#pragma once
#include<iostream>
#include<string>

namespace wyl
{
    class Task
    {
    private:
        int _x;
        int _y;
        char _op;
    public:
        Task(){}
        Task(int x,int y,int op)
        :_x(x),_y(y),_op(op)
        {}

        int Run()
        {
            int res = 0 ;
            switch(_op)
            {
                case '+':
                    res = _x + _y;
                    break;
                case '-':
                    res = _x - _y;
                    break;
                case '*':
                    res = _x * _y;
                    break;
                case '/':
                    res = _x / _y;
                    break;  
                case '%':
                    res = _x % _y;
                    break;
                default:
                    printf("bug??\n");
                    break;
            }
            printf("当前任务正在被%lu处理 ——> %d%c%d=%d\n",pthread_self(),_x,_op,_y,res);
            return res;
        }

        int operator()()
        {
            return Run();
        }

        std::string show()
        {
            std::string message;
            message +=  std::to_string(_x);
            message += _op;
            message +=  std::to_string(_y);
            message += "=?";
            return message;
        }
    };
}