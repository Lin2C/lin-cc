#include"Task.hpp"
#include"thread_pool.hpp"
#include<cstdlib>
#include<time.h>
#include<string>
#include<unistd.h>

using namespace wyl;


int main()
{
    srand((long long)time(nullptr));
    char ops[] = "+-*/%";
    //初始化线程池
    while(true)
    {
        //创建任务
        Task t(rand()%20+1,rand()%10+1,ops[rand()%5]);
        //派发给线程池
        thread_pool<Task>::GetInstance()->Push(t);
        std::cout<<thread_pool<Task>::GetInstance()<<std::endl;
        sleep(1);
    }

    return 0;
}