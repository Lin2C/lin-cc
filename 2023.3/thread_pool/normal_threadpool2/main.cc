#include"Task.hpp"
#include"thread_pool.hpp"
#include<cstdlib>
#include<time.h>
#include<string>
#include<unistd.h>

using namespace wyl;


int main()
{
    srand((long long)time(nullptr));
    char ops[] = "+-*/%";
    // thread_pool<Task>* tp = new thread_pool<Task>(3);
    // //初始化线程池
    // tp->pool_init();
    // while(true)
    // {
    //     //创建任务
    //     Task t(rand()%20+1,rand()%10+1,ops[rand()%5]);
    //     //派发给线程池
    //     tp->Push(t);
    //     sleep(1);
    // }


    //单例模式版本
    while(true)
    {
        Task t(rand()%20+1,rand()%10+1,ops[rand()%5]);
        thread_pool<Task>::GetThreadPool()->Push(t);
        sleep(1);
    }
    return 0;
}