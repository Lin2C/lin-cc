#define _CRT_SECURE_NO_WARNINGS 1

#include<iostream>
#include<algorithm>
#include<cstring>
using namespace std;

const int N = 12, M = 1 << N;
long long f[N][M];
bool st[M];
int m, n;

int main()
{
    while (cin >> n >> m, n || m)
    {
        //枚举出所有合法状态
        for (int i = 0; i < 1 << n; i++)
        {
            //判断合并了的i是否合法
            //横放表示1，竖放表示0
            //合并列不存在连续的奇数个0
            st[i] = true; //默认为合法状态
            int cnt = 0;//记录0的个数
            for (int j = 0; j < n; j++) //枚举每一行
            {
                if (i >> j & 1) //如果状态i的第j位(行)为1，不记录0的个数
                {
                    if (cnt & 1)
                    {
                        st[i] = false; //出现奇数个0。非法状态
                        break;
                    }
                }
                else cnt++;
            }
            if (cnt & 1)st[i] = false; //判断高位
        }
        //状态计算
        memset(f, 0, sizeof f);
        f[0][0] = 1;
        for (int i = 1; i <= m; i++) //枚举所有列
            for (int j = 0; j < 1 << n; j++) //枚举i的所有状态
                for (int k = 0; k < 1 << n; k++)//枚举i前一列的所有状态
                    // st[j | k] 俩个状态合并是否包含连续奇数个0，合法状态在上面计算过
                    if ((k & j) == 0 && st[j | k]) //k & j == 0,意思是和上列不重叠 
                        f[i][j] += f[i - 1][k]; // 从前一列的状态转移
        cout << f[m][0] << endl; //第m列不横放就是答案
    }

    return 0;
}