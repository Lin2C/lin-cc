#define _CRT_SECURE_NO_WARNINGS 1

#include<iostream>
using namespace std;

int n, m;
const int N = 1e6 + 10, M = 1e6 + 10;
char s[M], p[N];
int ne[M];

char* kmp()
{

	for (int i = 2, j = 0; i <= n; i++)
	{
		while (j && p[j + 1] != p[i]) j = ne[j];
		if (p[j + 1] == p[i]) j++;
		ne[i] = j;
	}
	for (int i = 1, j = 0; i <= m; i++)
	{
		while (j && p[j + 1] != s[i]) j = ne[j];
		if (p[j + 1] == s[i])j++;
		if (j == n)
		{
			return s + i - j + 1;
		}
	}
}


int main()
{
	cin >> n >> p + 1 >> m >> s + 1;
	printf("%s", kmp());

	return 0;
}