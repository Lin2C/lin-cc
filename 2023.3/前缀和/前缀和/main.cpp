#define _CRT_SECURE_NO_WARNINGS 1


#include<iostream>
#include<vector>
using namespace std;
const int N = 1e6 + 10;
int n, m;
int a[N], s[N];
int main()
{
    scanf("%d%d", &n, &m);
    for (int i = 1; i <= n; i++)  scanf("%d", &a[i]);
    for (int i = 1; i <= n; i++) s[i] = s[i - 1] + a[i];
    while (m--)
    {
        int r, l;
        scanf("%d%d", &l, &r);
        cout << s[r] - s[l - 1] << endl;
    }
    return 0;
}