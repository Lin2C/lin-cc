#define _CRT_SECURE_NO_WARNINGS 1

#include<iostream>
using namespace std;
const int N = 1e6 + 10;
int e[N], ne[N], head, idx;

void init()
{
	head = -1;
	idx = 0;
}

void add_to_head(int x)
{
	e[idx] = x, ne[idx] = head, head = idx++;
}
void remove(int k)
{
	ne[k] = ne[ne[k]]; //指向下下个节点，意味着删除了下一个节点
}

void add(int k, int x)
{
	e[idx] = x, ne[idx] = ne[k], ne[k] = idx++;
}

int main()
{
	init();
	add_to_head(1);
	add_to_head(2);
	add_to_head(3);
	add(1, 10);
	add(1, 20);
	add(1, 30);
	remove(1);
	for (int i = head; i != -1; i = ne[i]) printf("%d ", e[i]); printf("\n");
	remove(1);
	for (int i = head; i != -1; i = ne[i]) printf("%d ", e[i]); printf("\n");
	remove(1);
	for (int i = head; i != -1; i = ne[i]) printf("%d ", e[i]); printf("\n");
	return 0;
}