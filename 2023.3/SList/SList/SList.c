#define _CRT_SECURE_NO_WARNINGS 1
#include "SList.hpp"
#include<stdlib.h>
#include<assert.h>
#include<stdio.h>


// 动态申请一个节点
SListNode* BuySListNode(SLTDateType x)
{
	SListNode* newNode = (SListNode*)malloc(sizeof(SListNode));
	newNode->data = x;
	return newNode;
}
// 单链表打印
void SListPrint(SListNode* plist)
{
	SListNode* cur = plist;
	while (cur)
	{
		printf("%d->", cur->data);
		cur = cur->next;
	}
	printf("NULL\n");
}
// 单链表尾插
void SListPushBack(SListNode** pplist, SLTDateType x)
{
	assert(pplist);
	if (*pplist == NULL)
	{
		SListPushFront(pplist, x);
		return;
	}
	SListNode* tail = *pplist;
	while (tail->next)
	{
		tail = tail->next;
	}
	SListInsertAfter(tail, x);
}
// 单链表的头插
void SListPushFront(SListNode** pplist, SLTDateType x)
{
	assert(pplist);
	SListNode* newNode = BuySListNode(x);
	if (*pplist == NULL)
	{
		*pplist = newNode;
		newNode->next = NULL;
		return;
	}
	newNode->next = *pplist;
	*pplist = newNode;
}
// 单链表的尾删
void SListPopBack(SListNode** pplist)
{
	assert(pplist && *pplist);
	if ((*pplist)->next == NULL)
	{
		SListPopFront(pplist);
		return;
	}
	SListNode* tail = *pplist;
	SListNode* prev = NULL;

	while (tail->next)
	{
		prev = tail;
		tail = tail->next;
	}
	free(tail);
	prev->next = NULL;
}
// 单链表头删
void SListPopFront(SListNode** pplist)
{
	assert(pplist && *pplist);
	SListNode* next = (*pplist)->next;
	free(*pplist);
	*pplist = next;
}
// 单链表查找
SListNode* SListFind(SListNode* plist, SLTDateType x)
{
	SListNode* cur = plist;
	while (cur)
	{
		if (x == cur->data)
		{
			return cur;
		}
		cur = cur->next;
	}
	return NULL;
}
// 单链表在pos位置之后插入x
// 分析思考为什么不在pos位置之前插入？
void SListInsertAfter(SListNode* pos, SLTDateType x)
{
	assert(pos);
	SListNode* next = pos->next;
	SListNode* newNode = BuySListNode(x);
	pos->next = newNode;
	newNode->next = next;
}
// 单链表删除pos位置之后的值
// 分析思考为什么不删除pos位置？
void SListEraseAfter(SListNode* pos)
{
	assert(pos);
	SListNode* nextnext = pos->next->next;
	free(pos->next);
	pos->next = nextnext;
}
// 单链表的销毁
void SListDestroy(SListNode* plist)
{
	SListNode* cur = plist;
	SListNode* next = plist->next;
	while (cur)
	{
		next = cur->next;
		free(cur);
		cur = next;
	}
}