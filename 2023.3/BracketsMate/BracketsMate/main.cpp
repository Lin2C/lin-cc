#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
#include<stack>

bool isValid(std::string s) {
    std::stack<char> st;
    int i = 0;
    while (s[i])
    {
        if (s[i] == '(' || s[i] == '[' || s[i] == '{')
            st.push(s[i]);
        else if (s[i] == ')' || s[i] == ']' || s[i] == '}')
        {
            if (st.empty())
                return false;
            char ch = st.top();
            st.pop();
            if ((ch != '(' && s[i] == ')')
                || (ch != '[' && s[i] == ']')
                || (ch != '{' && s[i] == '}'))
                return false;
        }
        i++;
    }
    return st.empty();
}

int main()
{
    std::cout << isValid("()") << std::endl;
    return 0;
}