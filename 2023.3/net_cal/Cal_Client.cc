#include"Protocol.hpp"
#include"Socket.hpp"
#include<unistd.h>

using namespace wyl;

void Usage(string s)
{
    cout<<"Usage"<<s<<"ip  port"<<endl;
}


int main(int argc,char* argv[])
{
    if(argc != 3)
    {
        Usage(argv[0]);
        exit(0);
    }

    //1.创建套接字
    int sock = Sock::Socket();
    //2.连接服务器
    Sock::Connect(sock,atoi(argv[2]),string(argv[1]));

    //3.业务
    request_t req;
    memset(&req,0,sizeof(req));
    cout << "Please Enter Data One# ";
    cin >> req._x;
    cout << "Please Enter Data Two# ";
    cin >> req._y;
    cout << "Please Enter Operator# ";
    cin >> req._op;

    //4. 发送数据
    string req_string = SerializeRequest(req);
    ssize_t s = write(sock,req_string.c_str(),req_string.size());
    
   //ssize_t s = write(sock,&req,sizeof(req));

    //5.读取结果
    response_t resp;
    char buffer[1024];
    s = read(sock,buffer,sizeof(buffer)-1);
    if(s > 0)
    {
        buffer[s] = 0;
        string resp_string = buffer;
        DeserializeResponse(resp_string,&resp);
        cout<<"cond :" << resp._cond <<endl;
        cout<<"result :" << resp._result <<endl; 
    }
    else
    {
        cout<<"数据加载失败"<<endl;
    }
    // if(s == sizeof(resp))
    // {
    //     //数据没有丢失
    //     cout<<"cond :" << resp._cond <<endl;
    //     cout<<"result :" << resp._result <<endl; 
    // }

    return 0;
}