#include"Socket.hpp"
#include"Protocol.hpp"
#include<unistd.h>

void Usage(string s)
{
    cout<<"Usage"<<s<<" port"<<endl;
}

void* HandlerRequest(void* args)
{
    //1.分离线程
    pthread_detach(pthread_self());
    //2.提取套接字
    int sock = *(int*)args;
    delete (int*)args;
   
    //提供短服务
    //3.获取请求数据
    // request_t req;
    // ssize_t s = read(sock,&req,sizeof(req));
    // response rpe;
    // memset(&rpe,0,sizeof(rpe));
    request_t req;
    char buffer[1024];
    ssize_t s = read(sock,buffer,sizeof(buffer)-1);
    if(s > 0)
    {
        buffer[s] = 0;
        string req_string = buffer;
        DeserializeRequest(req_string,&req);
         //4.提供服务
        response rpe;
        switch(req._op)
        {
            case '+':
                rpe._result = req._x + req._y;
                break;
            case '-':
                rpe._result = req._x - req._y;
                break;
            case '*':
                rpe._result = req._x * req._y;
                break;
            case '/':
                if(req._y == 0)
                    rpe._cond = -1;
                else
                    rpe._result = req._x / req._y;
                break;
            case '%':
                if(req._y == 0)
                    rpe._cond = -2;
                else
                    rpe._result = req._x % req._y;
                break;
            default :
                rpe._cond = -3;
                break;
        }
        cout << "request: " << req._x << req._op << req._y << endl;
        //5.发送响应
        string sen_reps = SerializeResponse(rpe);
        write(sock,sen_reps.c_str(),sen_reps.size());

        //5.发送响应
        //write(sock,&rpe,sizeof(rpe));
    }
    
    // //4.提供服务
    // if(s == sizeof(req))
    // {
    //     //没有发生数据丢失的情况
    //     switch(req._op)
    //     {
    //         case '+':
    //             rpe._result = req._x + req._y;
    //             break;
    //         case '-':
    //             rpe._result = req._x - req._y;
    //             break;
    //         case '*':
    //             rpe._result = req._x * req._y;
    //             break;
    //         case '/':
    //             if(req._y == 0)
    //                 rpe._cond = -1;
    //             else
    //                 rpe._result = req._x / req._y;
    //             break;
    //         case '%':
    //             if(req._y == 0)
    //                 rpe._cond = -2;
    //             else
    //                 rpe._result = req._x % req._y;
    //             break;
    //         default :
    //             rpe._cond = -3;
    //             break;
    //     }
    //     cout << "request: " << req._x << req._op << req._y << endl;
    //     //5.发送响应
    //     write(sock,&rpe,sizeof(rpe));
    // }

    //6.关闭文件
    close(sock);
}


using namespace wyl;

int main(int argc,char* argv[])
{
    if(argc != 2)
    {
        Usage(argv[0]);
        exit(0);
    }
    //创建套接字
    int listen_sock = Sock::Socket();
    //绑定套接字
    Sock::Bind(listen_sock,atoi(argv[1]));

    //设置监听状态
    Sock::Listen(listen_sock);

    while(true)
    {
        //接收连接
        int new_sock = Sock::Accept(listen_sock);
        if(new_sock > 0)
        {
            //等到了一个连接
            cout << "get a new client..." << endl;
            pthread_t tid;
            int* pram = new int(new_sock); 
            pthread_create(&tid,nullptr,HandlerRequest,(void*)pram);// 创建线程提供服务
        }
    }


    return 0;
}