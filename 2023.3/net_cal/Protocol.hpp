#pragma once
#include<iostream>
#include<string>
#include<jsoncpp/json/json.h>
using namespace std;


typedef struct request
{
    int _x;
    int _y;
    char _op;
}request_t;


typedef struct response
{
    int _cond;
    int _result;
}response_t;

//序列化请求
string SerializeRequest(const request_t &req)
{
    Json::Value root;
    root["datax"] = req._x;
    root["datay"] = req._y;
    root["opertaor"] = req._op;
    Json::FastWriter Write;
    string ret = Write.write(root);

    return ret;
}

//反序列化请求
void DeserializeRequest(const string& json_string,request_t* out)
{
    Json::Reader reader;
    Json::Value root;
    reader.parse(json_string,root);

    out->_x = root["datax"].asInt();
    out->_y = root["datay"].asInt();
    out->_op = (char)root["opertaor"].asInt();
}

//序列化响应
string SerializeResponse(const response_t &req)
{
    Json::Value root;
    root["cond"] = req._cond;
    root["result"] = req._result;
    Json::FastWriter Write;

    return Write.write(root);
}

//反序列化响应
void DeserializeResponse(const string& json_string,response_t* out)
{
    Json::Reader reader;
    Json::Value root;
    reader.parse(json_string,root);
    out->_cond =  root["cond"].asInt();
    out->_result = root["result"].asInt();
}
