#include "Sock.hpp"
#include <pthread.h>
#include <stdlib.h>
#include <cstring>
#include <unistd.h>
#include <fstream>
#include <sys/stat.h>

using namespace std;
using namespace wyl;

#define WWWROOT "./wwwroot/"
#define NAME "index.htmlAA"

// 处理线程
void *HandlerHttpRequest(void *args)
{
    int sock = *(int *)args;
    delete (int *)args;
    // 提供短服务
#define NUM 1024 * 10
    char buffer[NUM];
    memset(buffer, 0, sizeof(buffer));
    ssize_t s = recv(sock, buffer, sizeof(buffer) - 1, 0);

    if (s > 0)
    {
        //设置重定向
        string resp_string = "http/1.0 301 Permanently moved\n";
        resp_string += "Location: https://www.gov.cn/\n" ;
        resp_string += "\n";
        send(sock, resp_string.c_str(), resp_string.size(), 0);

        // {
        //     //读取成功
        //     cout<<buffer;

        //     //给客户端发送响应
        //     std::string resp_string = "http/1.0 200 OK\n"; //响应
        //     resp_string += "Content-Type: text/plain\n";//text/plain 是普通文本
        //     resp_string += "\n" ; //空行
        //     resp_string += "hello,my friend!";
        //     send(sock,resp_string.c_str(),resp_string.size(),0);
        // }
        // else
        // {
        //     cout<<"recv failed"<<endl;
        // }

    //     string file_html = WWWROOT;
    //     file_html += NAME;
    //  //   cout<<file_html<<endl;
    //     ifstream in(file_html);

    //     if (!in.is_open())
    //     {
    //         // 打开失败
    //         string resp_string = "http/1.0 404 no found\n";
    //         resp_string += "Content-Type: text/html;charset=utf8\n";
    //         resp_string += "\n"; // 空行
    //         resp_string += "<html><p>你访问的资源不存在</p></html>";
    //         send(sock,resp_string.c_str(),resp_string.size(),0);
    //     }
    //     else
    //     {
    //         // 打开成功
    //         std::string resp_string = "http/1.0 200 OK\n"; // 响应
    //         resp_string += "Content-Type: text/html;charset=utf8\n";

    //         // 正文
    //         struct stat st;
    //         stat(file_html.c_str(), &st);         // 获取一个文件的信息，输出型参数。
    //         resp_string += "Content-Length: ";    // 正文长度
    //         resp_string += to_string(st.st_size); // 加上长度
    //         resp_string += "\n"; // 空行
    //         resp_string += "\n"; // 空行

    //         // 正文,一行一行获取
    //         string content;
    //         string line;
    //         while (getline(in, line))
    //             content += line;
    //         resp_string += content;
    //         send(sock, resp_string.c_str(), resp_string.size(), 0);
    //         in.close();
    //     }
     }
    close(sock);
    return nullptr;
}

void Usage(string us)
{
    cout << "Usage ： " << us << " port" << endl;
}

int main(int argc, char *argv[])
{
    if (argc != 2)
    {
        Usage(argv[0]);
    }

    // 创建
    int listen_sock = Sock::Socket();
    // 绑定
    Sock::Bind(listen_sock, atoi(argv[1]));
    // 监听
    Sock::Listen(listen_sock);

    while (true)
    {
        // 等待连接
        int new_sock = Sock::Accpet(listen_sock);
        int *parem = new int(new_sock);
        // 创建线程
        pthread_t tid;
        pthread_create(&tid, nullptr, HandlerHttpRequest, (void *)parem);
    }

    return 0;
}
