#include<stdio.h>
#include<unistd.h>
#include<fcntl.h>
#include<string.h>
#include<stdlib.h>

int main()
{
  int fds[2];
  int ret  = pipe(fds); 
  if(ret != 0)
  {
    perror("pipe fail\n");
    return 1;
  }
  
  if(fork() == 0)
  {
    //child   write
    close(fds[0]); 
    const char* mesg = "hello linux";
    int count = 0;
    while(1)
    {
      write(fds[1],"c",1);
      printf("%d\n",count++);
     // sleep(1);
    }
    exit(0);
  }
  //father  read 
  close(fds[1]);
  while(1)
  {
    sleep(10);
    char buff[1024*2+1] = {0}; 
    int s = read(fds[0],buff,sizeof(buff) - 1);
    buff[s] = 0;
    if(s > 0)
    {
      printf("child say to father # a\n");
    
    }else if(s == 0)
    {
      printf("child quiet....\n");
      break;
    }
    else{
      printf("read fail\n");
      break;
    }
  }

  return 0;
}
