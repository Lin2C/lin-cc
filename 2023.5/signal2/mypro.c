#include<stdio.h>
#include<signal.h>
#include<stdlib.h>
#include<unistd.h>
#include<string.h>

void show(sigset_t* set)
{
  int i = 0;
  for(i  =1;  i <= 31 ; i++)
  {
    if(sigismember(set,i))
      printf("1");
    else printf("0");
  }
  printf("\n");
}

int main()
{

  sigset_t set,p; //定义信号集

  sigemptyset(&set); //初始化信号集
  sigemptyset(&p); 

  sigaddset(&set,2); //往set信号集添加一个2号信号
  sigprocmask(SIG_SETMASK,&set,NULL) ; //设置屏蔽信号集为set
  while(1)
  {
    sigpending(&p); //获取信号集
    show(&p); //打印信号集
    sleep(1);
  }
  return 0;
}
