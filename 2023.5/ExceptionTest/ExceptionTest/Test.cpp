#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
#include<string>
#include<Windows.h>
#include<time.h>
#include<cstdlib>
using namespace std;

class Exception
{
public:
	Exception(string mes, int id) : message_(mes), id_(id) {}

	virtual string What() const
	{
		return message_;
	} 
	int getid() const
	{
		return id_;
	}
protected :
	string message_;
	int id_;
};

class Exc1 : public Exception
{
public:
	Exc1(string mes, int id) : Exception(mes, id){}
	virtual string What() const
	{
		string mess = "Exc1 �쳣......, type : ";
		mess += message_;
		mess += "  id:";
		mess += to_string(id_);
		return mess;
	}
	int getcnt()
	{
		return cnt_;
	}
	void subcnt()
	{
		cnt_--;
	}
private:
	int cnt_ = 3;
};

class Exc2 : public Exception
{
public:
	Exc2(string mes, int id) : Exception(mes, id) {}
	virtual string What() const
	{
		string mess = "Exc2 �쳣......, type :";
		mess += message_;
		mess += "  id:";
		mess += to_string(id_);
		return mess;
	}
};

void seed()
{
	if (rand() % 3 == 0) 
	{
		throw Exc1("Exc1", 100);
	}
	if (rand() % 4 == 0)
	{
		throw Exc2("Exc2", 200);
	}
}

void Httpserver()
{
	int n = 3;
	while (n--)
	{
		try
		{
			seed();
		}
		catch(const Exception& e)
		{
			if (e.getid() == 100 && n)
			{
				continue;
			}
			else
			{
				throw e;
			}
		}
	}
}


int main()
{
	srand(time(0));
	while (1)
	{
		Sleep(1000);
		try
		{
			Httpserver();
		}
		catch(const Exception& e)
		{
			cout << e.What() << endl;
		}
		catch (...)
		{
			cout << "unknow err" << endl;
		}
	}
	return 0;
}