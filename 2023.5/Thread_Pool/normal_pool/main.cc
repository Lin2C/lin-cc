#include<iostream>
#include<pthread.h>
#include"Thread_Pool.hpp"
#include"Task.hpp"
#include<stdlib.h>
#include<time.h>
#include<unistd.h>
using std::cout;

using namespace wyl_thread_pool;
using namespace wyl_task;

void* ProducedTask(void* args)
{
    Thread_Pool<Task>* pool  = (Thread_Pool<Task>*)args;
    while(true)
    {
        sleep(1);
        Task t = Task(rand()%20+1 , rand()%10+1 , "+-*/%"[rand()%5]);
       // std::cout << t.Show() << std::endl;
        pool->push(t);
    }
}

int main()
{
    Thread_Pool<Task>* pool = new Thread_Pool<Task>();
    pool->InitThreadPool();
    srand(time(0));
    
    //创建生产任务的线程
    pthread_t tids[5];
    for(int i =  0;  i < 5 ; i ++)
        pthread_create(tids+i,nullptr,ProducedTask,(void*)pool);

    for(int i = 0 ; i < 5 ; i++)
        pthread_join(tids[i],nullptr);

    return 0;
}