#pragma once
#include<string>
#include<iostream>

namespace wyl_task
{
    class Task
    {
    public:
        Task(){}
        Task(int x, int y , char op):x_(x) , y_(y) , op_(op){}
        
        std::string Show()
        {
            std::string message;
            message += std::to_string(x_);
            message += op_;
            message += std::to_string(y_);
            return message;
        }
        int Run()
        {
            int ret;
            switch(op_)
            {
                case '+':  
                    ret = x_ + y_;
                        break; 
                case '-':
                    ret = x_ - y_; 
                        break;             
                case '*':
                    ret = x_ * y_; 
                        break;      
                case '/':
                    ret  =x_ / y_; 
                        break; 
                case '%':
                    ret = x_ % y_; 
                        break;
                default :
                std::cout  << " bug ? op_ = " << op_ << std::endl;
            }
            printf("当前任务正在被: %ld , 处理: %d %c %d = %d\n",pthread_self(),x_,op_,y_,ret);
            return ret;
        }
        
        int operator()()
        {
            return Run();
        }

    private:
        int x_;
        int y_;
        char op_;

    };

};