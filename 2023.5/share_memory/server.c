#include"common.h"


int main()
{
  key_t Key = ftok(PATH_NAME,KEY_ID);
  //获取创建共享内存
  int shmid = shmget(Key,4096,IPC_CREAT | IPC_EXCL | 0666) ; //如果共享内存不存在创建，存在就失败
  if(shmid < 0)
  {
    perror("shmget fail");
    return 1;
  }
 
  char* s = (char*)shmat(shmid,NULL,0);
  if( s  ==(void*)-1)
  {
    perror("shmat fail");
    return 2;
  }
  while(1)
  {
    sleep(1);
    printf("client say : %s \n",s);
  }

  shmdt(s);

  printf("Key : %d shmid :  %d",Key,shmid);
  

}
