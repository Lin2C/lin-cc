#include<iostream>
#include"block_queue.h"
#include<time.h>
#include<cstdlib>
#include<unistd.h>

using std::cout;
using std::endl;
using namespace wyl;

void* ConsumerRun(void* args)
{
    BlockQueue<int>* q = (BlockQueue<int>*)args;
    while(1)
    {
        int out;
        q->pop(&out);
        cout << "消费者消费了一条数据：" << out << endl;
        sleep(1);
    }
}

void* ProducerRun(void* args)
{
    BlockQueue<int>* q = (BlockQueue<int>*)args;
    while(1)
    {
        int r = rand() % 20 + 1;
        q->push(r);
        cout << "生产者生产了一个数据: " << r << endl;
    }
}

int main()
{
    BlockQueue<int>* q = new BlockQueue<int>();
    srand(time(0));
    pthread_t consumer , producer;
    pthread_create(&consumer,nullptr,ConsumerRun,(void*)q);
    pthread_create(&producer,nullptr,ProducerRun,(void*)q);

    pthread_join(consumer,nullptr);
    pthread_join(producer,nullptr);
    return 0;
}