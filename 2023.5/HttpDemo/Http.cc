#include<iostream>
#include<string>
#include"Sock.hpp"
#include<pthread.h>
#include<unistd.h>
#include<fstream>
#include<sys/types.h>
#include<sys/stat.h>
#include<unistd.h>

using namespace wyl;
#define WWWROOT "./wwwroot/"
#define PATH_INDEX "index.html" 


/* 请求一个文本
void* Running(void* argv)
{
    int sock = *(int*)argv;
    delete (int*)argv;
    //提供服务
    std::string req;
#define H_SIZE 1024*10
    char buff[H_SIZE];
    //接收请求
    ssize_t s = recv(sock,buff,sizeof buff - 1, 0);
    if(s > 0)
    {
        buff[s] = 0;
        req += buff;
        std::cout << req << std::endl;
    
        //响应请求
        std::string res;
        res += "http/1.0 200 ok\n"; //响应头
        res += "Content-Type: text/plain\n"; //响应报文
        res += "\n"; //空行
        //找到User-Agent:字段
        int post = req.find("User-Agent:");
        post += 10;
        res += "ni de she bei xing hao shi：";
        while(req[post] != '\n')
        {
            res += req[post];
            post++;
        }
        res += "\n";

        send(sock,res.c_str(),res.size() , 0);
    }
    close(sock);
}
*/


//请求一个html页面

void* Running(void* argv)
{
    int sock = *(int*)argv;
    delete (int*)argv;
    //提供服务
    std::string req;
#define H_SIZE 1024*10
    char buff[H_SIZE];
    //接收请求
    ssize_t s = recv(sock,buff,sizeof buff - 1, 0);
    if(s > 0)
    {
        buff[s] = 0;
        req += buff;
        std::cout << req << std::endl;
    
        //响应请求
        std::string res;
        std::string html_file = WWWROOT;
        html_file += PATH_INDEX;
        //打开文件

        std::ifstream in(html_file);
        if(!in.is_open())
        {
            //打开文件失败
            res += "http/1.0 404 not found\n";
            res += "Content-Type: text/html;charset=utf-8\n";
            res += "\n";
            res += "<html><p>你访问的资源不存在</p></html>";
        }
        else
        {
            //打开文件成功
            res += "http/1.0 200 ok\n";
            struct stat file;
            stat(html_file.c_str(),&file);
            res += "Content-Type: text/html;charset=utf-8\n";
            res += "Content-Length:";
            res += std::to_string(file.st_size); 
            res += "\n";
            res += "\n"; //空行

            //加上正文 ， html整个文件内容
            std::string line;
            while(std::getline(in,line))
            {
                res += line;
            }
            res += "\n";
        }

        send(sock,res.c_str(),res.size() , 0);
        in.close();
    }
    close(sock);
}


int main(int argc , char* argv[])
{
    if(argc != 2)
    {
        std::cout << argv[0] << " server_port" << std::endl;
        return 1;
    }
    int listensock = Sock::Socket();
    Sock::Bind(listensock,atoi(argv[1]));
    Sock::Listen(listensock);

    while(true)
    {
        int new_sock = Sock::Accept(listensock);
        if(new_sock < 0 )
        {
            std::cout << " accept error" << std::endl;
            continue;
        }

        pthread_t pid;
        int* ssock = new int(new_sock);
        pthread_create(&pid,nullptr,Running,(void*)ssock);
    }
    return 0;

}