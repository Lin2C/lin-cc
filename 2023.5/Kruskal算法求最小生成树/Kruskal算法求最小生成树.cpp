#include<iostream>
#include<cstring>
#include<algorithm>
using namespace std;
const int N = 1e5+10 , M = 1e5 * 2 + 10;

struct Edge
{
    int a, b , w;
    bool operator<(const Edge& W)const
    {
        return w < W.w;
    }
}edge[M];
int n , m , p[N];

int find(int x)
{
    if(x != p[x]) p[x] = find(p[x]);
    return p[x];
}

int main()
{
    cin >> n >> m;
    for(int i = 0 ; i < m ; i++)
    {
        int a ,b ,w ;
        cin >> a >> b >> w;
        edge[i] = { a ,b , w};
    }
    sort(edge,edge + m);
    for(int i = 1; i <= n ;  i++) p[i] = i;
    int cnt = 0 , sum =  0;
    for(int i = 0 ; i < m;  i++)
    {
        int a = edge[i].a , b = edge[i].b , w = edge[i].w;
        a = find(a) , b = find(b);
        if(a != b)
        {
            p[a] = b;
            cnt ++ ;
            sum += w;
        }
    }
    if(cnt < n - 1) puts("impossible");
    else cout << sum << endl;
    return 0;
}