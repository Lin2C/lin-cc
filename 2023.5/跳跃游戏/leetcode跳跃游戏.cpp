class Solution {
public:
    bool canJump(vector<int>& nums) {
        int n = nums.size();
        if(n == 1) return true;
        if(nums[0] == 0) return false;
        for(int i = 0; i< n - 1 ; i++)
            if(nums[i] == 0)
            {
                bool falg = false;
                for(int j = i - 1 ; j >= 0 ; j--)
                    if(nums[j] + j > i ) falg = true;
                if(!falg) return false;
            }
        return true;
    }
};