#include<iostream>
#include<sys/socket.h>
#include<sys/types.h>
#include<netinet/in.h>
#include<arpa/inet.h>
#include<cstring>

void usage(char* s)
{
    std::cout << s << " server_ip server_port" << std::endl;
}
 
int main(int argc , char* argv[])
{
    if(argc != 3)
    {
        usage(argv[0]);
        return 2;
    }

    //create socket
    int sock = socket(AF_INET,SOCK_DGRAM,0);
    if(sock < 0)
    {
        std::cout << "socket error" << std::endl;
        return 1;
    }

    char buff[1024];
    struct sockaddr_in server;
    server.sin_family = INADDR_ANY;
    server.sin_port = htons(atoi(argv[2]));
    server.sin_addr.s_addr = inet_addr(argv[1]);
    while(true)
    {
        //input 
        std::cout << "输入#";
        std::cin >> buff;
        //send
        sendto(sock,buff,strlen(buff),0,(struct sockaddr*)&server,sizeof(server));
        //receive
        struct sockaddr_in tmp;
        socklen_t len = sizeof(tmp);
        recvfrom(sock,buff,sizeof(buff),0,(struct sockaddr*)&tmp,&len);
        std::cout << "server #" << buff << std::endl;
    }
    return 0;
}