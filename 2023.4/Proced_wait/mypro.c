#include<stdio.h>
#include<unistd.h>
#include<stdlib.h>
#include<sys/wait.h>

int main()
{
  int p = fork();
  if(p == 0)
  {
    int cnt =5;
    while(cnt)
    {
      printf("child is runing ,cnt is %d\n",cnt);
      cnt--;
      sleep(1);
    }
    int a = 10;
    a /= 0;
    exit(12);
  }
  sleep(10);
  int status = 0;
  printf("father begin waiting\n");
  waitpid(p,&status,0);
  if(status < 0)
  {
    //fail
    printf("exit code is %d , sign is %d\n",(status >> 8)&0xff, status & 0x7f);
  }
  else 
  {
    
    printf("exit code is %d , sign is %d\n",(status >> 8)&0xff, status & 0x7f);
  }
  
  return 0;

}
