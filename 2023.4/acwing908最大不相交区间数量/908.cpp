#include<iostream>
#include<algorithm>
using namespace std;

const int N = 100010;
struct Range
{
    int l ,r ;
    bool operator<(const Range& W) const
    {
        return r < W.r;
    }
}range[N];
int n;
int main()
{
    cin >> n;
    for(int i = 0 ; i < n ; i++) cin >> range[i].l >> range[i].r;
    sort(range,range+n);
    int end = -2e9,ans = 0;
    for(int i = 0 ; i < n ; i++)
        if(range[i].l > end)
        {
            ans++;
            end = range[i].r;
        }

    cout << ans << endl;
    return 0;
}