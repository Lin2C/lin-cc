#define _CRT_SECURE_NO_WARNINGS 1
////
////#include<stdlib.h>
////#include<string.h>
////#include<stdio.h>
//////#include<>
////
////typedef struct list
////{
////	int val;
////	struct list* next;
////}list;
////
////
////list* getNode(int val)
////{
////	list* node = (list*)malloc(sizeof(list)); //开辟一个节点
////	node->next = NULL; //开辟节点指向空
////	node->val = val; // 这个节点存储的值
////	return node; 
////}
////
////
////void print_list(list* head)
////{
////	head = head->next;
////	while (head)
////	{
////		printf("%d->", head->val);
////		head = head->next ;
////	}
////	printf("NULL\n");
////}
////
////void push_front(list* head, int x)
////{
////	list* newNode = getNode(x); //获取/创建节点
////	newNode->next = head->next; //创建节点指向头节点的下一个
////	head->next = newNode;
////}
////
////void push_back(list* head, int x)
////{
////	list* newNode = getNode(x);
////	list* cur = head;
////	while (cur->next)
////	{
////		cur = cur->next;
////	}
////	cur->next = newNode;
////}
////
////int main()
////{
////	list* head = getNode(5);
////	push_back(head, 1);
////	push_back(head, 2);
////	push_back(head, 3);
////	//push_front(head, 1);
////	//push_front(head, 2);
////	//push_front(head, 3);
////	//push_front(head, 4);
////	print_list(head);
////	return 0;
////}
//
//
//
//#include<stdio.h>
//
////ABC 和 XYZ 两个球队打球， A不和X ， C 不和 X&Z
//
//int main()
//{
//	for (char A = 'X'; A <= 'Z'; A++) //A和谁打
//		for (int B = 'X'; B <= 'Z'; B++) //B和谁打
//			if (A != B) //A 和 B不相同的意思就是 A和B的对手不能相同
//				for (int C = 'X'; C <= 'Z'; C++) //C和谁打
//					if (C != B && A != C && A != 'X' && C != 'X' && C != 'Z')
//						printf("A->%c, B->%c, C->%c\n", A, B, C);
//
//	return 0;
//}


#include<string.h>
#include<iostream>
using std::endl;
using std::cout;

namespace wyl
{
    class My_String
    {
    public:
        //构造函数  
        My_String(const char* str = "")
        {
            _size = strlen(str);
            _capacity = _size;
            _str = new char[_capacity + 1];
            strcpy(_str,str); 
        }

        //功能实现
        char* c_str()
        {
            return _str; 
        }

    private:
        size_t _capacity; //string 容量
        size_t _size; //字符串长度
        char* _str; //存储
    };

    // operator<<()

    void Test()
    {
        My_String str("abcd");
        cout << str.c_str() << endl;
    }
}


int main()
{
    wyl::Test();
    return 0;
}