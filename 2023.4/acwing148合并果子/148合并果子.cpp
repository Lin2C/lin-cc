#include<iostream>
#include<algorithm>
#include<queue>
using namespace std;

const int N = 20010;

int main()
{
    int n;
    int ans = 0 ;
    cin >> n;
    priority_queue<int,vector<int>,greater<int>> heap;
    for(int i = 0 ; i < n ; i ++)
    {
        int a;
        cin >> a;
        heap.push(a); //入堆
    }
    while(heap.size() > 1)
    {
        //取出个最小值加上答案并且入堆
        int a = heap.top();
        heap.pop();
        int b = heap.top();
        heap.pop();
        ans += a+b;
        heap.push(a+b);
    }
    cout << ans << endl;
    return 0;
}