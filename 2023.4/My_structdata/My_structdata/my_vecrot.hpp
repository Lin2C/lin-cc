#define _CRT_SECURE_NO_WARNINGS 1

#include<stdlib.h>
#include<assert.h>
namespace wyl
{
	
	template<class T>
	class vector
	{
		const int N = 4;
	public:
		//迭代器
		vector()
		{
			_start = (T*)malloc(sizeof(T) * N);
			assert(_start);
			_finsh = _start;
			_endofstorage = _start + N;
		}
		//操作符重载
		//[]重载
		T& operator[](int x)
		{
			return *(_start + x);
		}
		const T& operator[](int x) const
		{
			return *(_start + x);
		}
		
		//大小
		size_t size()
		{
			return _finsh - _start;
		}
		//尾插
		void push_back(const T& val)
		{
			if (_finsh == _endofstorage)
				reserve(size() * 2);
			
			*_finsh = val;
			_finsh++;
		}
		//尾删
		void pop_back()
		{
			assert(_start != _finsh);
			_finsh--;
		}
		//重置容量
		void reserve(const size_t Size)
		{
			size_t len = size(); //备份一下当前长度
			_start = (T*)realloc(_start,Size * sizeof(T));
			//如果空间被重新开辟			
			_finsh = _start + len;
			_endofstorage = _start + Size ;
		}
		//重置大小
		void resize(const size_t Sz, const T& val)
		{
			//重置大小比实际容量小
			if (Sz > _endofstorage - _start)
			{
				reserve(Sz);

			}
			else
			{

			}
			
		}
	private:
		T* _start;
		T* _finsh;
		T* _endofstorage;
	};


	void print(vector<int>& v)
	{
		for (int i = 0; i < v.size(); i++)
			std::cout << v[i];
		std::cout << std::endl;
	}

	void vector_test()
	{
		vector<int> v;
		v.push_back(1);
		v.push_back(2);
		v.push_back(3);
		//std :: cout << v.size() << std::endl;
		v.push_back(4);
		v.push_back(5);
		v.push_back(6);
		print(v);
		v.pop_back();
		print(v);
		v.pop_back();
		print(v);
		v.pop_back();
		print(v);


	}
}