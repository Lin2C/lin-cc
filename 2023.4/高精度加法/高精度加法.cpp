#include<stdio.h>
#include<string.h>


void Reverse(char* str)
{
	int l = 0, r = strlen(str) - 1;
	while (l < r)
	{
		char tmp = str[l];
		str[l] = str[r];
		str[r] = tmp;
		l++, r--;
	}
}

void add(char* A, char* B,char* C)
{
	int t = 0;
	int j = 0;
	for (int i = 0; i < strlen(A) || i < strlen(B); i++)
	{
		if (i < strlen(A))
			t += A[i] - '0';
		if (i < strlen(B))
			t += B[i] - '0';
		C[j++] = (t % 10) + 48;
		t /= 10;
	}
	if (t) C[j] = t + 48;
	Reverse(C);
}
int main()
{
	char A[10000] = { 0 };
	char B[10000] = { 0 };
	char C[10001] = { 0 };
	scanf("%s%s", A, B);
	Reverse(A);
	Reverse(B);
	add(A, B,C);
	printf("%s\n", C);
}
