#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
#include<algorithm>
using namespace std;

const int N = 1e5 + 10;
int n;
struct Range
{
    int l, r;
    bool operator<(const Range& W) const
    {
        return r < W.r; //按右端点排序
    }
}Ranges[N];

int main()
{
    int res = 0;  //答案
    int end = -2E9; //上一个区间的最后一个点的值
    cin >> n;
    for (int i = 0; i < n; i++) cin >> Ranges[i].l >> Ranges[i].r;

    sort(Ranges, Ranges + n); //区间排序
    //枚举所有区间
    for (int i = 0; i < n; i++)
        if (Ranges[i].l > end) //如果当前区间的左端点大于上一个区间的右端点，说明2个区间没有并集，点数+1
        {
            end = Ranges[i].r;
            res++;
        }

    cout << res << endl;
    return 0;
}

