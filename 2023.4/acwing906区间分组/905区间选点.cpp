#include<iostream>
#include<algorithm>
#include<queue>
#include<vector>
using namespace std;
const int N = 100010;
int n ;
struct Range{
    int l ,r ;
    bool operator<(const Range& W) const 
    {
        return l < W.l;
    }
}range[N];

int main()
{
    cin >> n;
    for(int i = 0 ; i < n ; i++) cin >> range[i].l >> range[i].r;
    
    sort(range,range+n); //区间排序，左端点排序
    priority_queue<int , vector<int>,greater<int>> heap; //创建一个堆，存储每个组的max_r
    
    //枚举所有区间
    for(int i = 0  ; i< n ; i++)
    {
        auto t = range[i];
        if(heap.empty() || heap.top() >= t.l) heap.push(t.r); //如果和右端点最小的区间存在交集，那么创建一个新的组
        else
        {
            //不存在交集，
            heap.pop();
            heap.push(t.r);
        }
    }
    cout << heap.size()<<endl;
    return 0;
}