#pragma once
#include<iostream>

namespace wyl
{
	class Date
	{
	public:
		Date(int year = 1, int month = 1, int day = 1);

		bool operator<(const Date& d);
		bool operator==(const Date& d);
		bool operator<=(const Date& d);
		bool operator>(const Date& d);
		bool operator>=(const Date& d);
		bool operator!=(const Date& d);
		Date operator+(int days);
		Date& operator+=(int days);
		Date operator-(int days);
		Date& operator-=(int days);

		int GetMonthDay(int y , int m)
		{
			static int months[13] = { 0,31,28,31,30,31,30,31,31,30,31,30,31 };
			if (m == 2 && (y % 400 == 0 || (y % 4 == 0 && y % 100 != 0)))
				return 29;
			return months[m];
		}
		void Pirntf()
		{
			std::cout << year_ << "/" << month_ << "/" << day_ << std::endl;
		}


	private:
		int year_ ;
		int month_;
		int day_ ;
	};

	void DateTest();
}