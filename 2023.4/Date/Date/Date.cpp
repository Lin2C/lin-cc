#define _CRT_SECURE_NO_WARNINGS 1
#include"Date.h"


wyl::Date::Date(int year, int month, int day)
{
	year_ = year;
	month_ = month;
	day_ = day;
}

bool wyl::Date::operator<(const Date& d)
{
	if (year_ != d.year_) return year_ < d.year_;
	if (month_ != d.month_) return month_ < d.month_;
	if (day_ != d.day_) return day_ < d.day_;
	return false;
}

bool wyl::Date::operator==(const Date& d)
{
	return year_ == d.year_
		&& month_ == d.month_
		&& day_ == d.day_;
}

bool wyl::Date::operator<=(const Date& d)
{
	return *this < d || *this == d;
}
bool wyl::Date::operator>(const Date& d)
{
	return !(*this <= d);
}
bool wyl::Date::operator>=(const Date& d)
{
	return !(*this < d);
}

bool wyl::Date::operator!=(const Date& d)
{
	return !(*this == d);
}

wyl::Date& wyl::Date::operator+=(int days)
{
	int t = GetMonthDay(year_, month_); //获取当前月份天数
	day_ += days;
	while (t < day_)
	{
		day_ -= t ;
		++month_;
		if (month_ == 13)
		{
			++year_;
			month_ = 1;
		}
		t = GetMonthDay(year_, month_ );
	}
	return *this;
}

wyl::Date wyl::Date::operator+(int days)
{
	Date tmp(*this);
	tmp += days;
	return tmp;
}

wyl::Date wyl::Date::operator-(int days)
{
	Date tmp(*this);
	tmp -= days;
	return tmp;
}

wyl::Date& wyl::Date::operator-=(int days)
{
	if (days < 0)
	{
		day_ += -days;
		return *this;
	}
	day_ -= days;
	int t = GetMonthDay(year_, month_  );
	while (day_ < 0)
	{
		month_--;
		if (month_ == 0)
		{
			month_ = 12;
			--year_;
		}
		day_ += GetMonthDay(year_, month_ );
	}
	return *this;
}

void wyl::DateTest()
{
	Date d1(2023, 4, 28);
	Date d2(2020, 2, 15);
	//if (d1 != d2)
	//	std::cout << "AAAA" << std::endl;
	d1 -= 200;
	d1.Pirntf();

}