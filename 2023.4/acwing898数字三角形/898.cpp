#include<iostream>
#include<cstring>
using namespace std;
const int N = 510;
int f[N][N];
int w[N][N];

int main()
{
    int n ; 
    cin >> n ;
    for(int i = 1;  i<= n ; i++)
        for(int j =1 ; j <= i ; j++) cin >> w[i][j];
    
    memset(f,-0x3f,sizeof f);
    f[1][1] = w[1][1];
    for(int i = 2 ; i <= n ; i++)
        for(int j = 1 ; j <= i ; j ++) f[i][j] = max(f[i-1][j], f[i-1][j-1]) + w[i][j];
    int ans = -0x3f3f3f3f;
    for(int i  =1;  i<= n ; i++) ans = max(ans, f[n][i]);
    cout << ans << endl;
    return 0;
}