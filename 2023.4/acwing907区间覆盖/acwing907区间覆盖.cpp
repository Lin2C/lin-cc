#include<iostream>
#include<algorithm>
using namespace std;

const int N = 100010;
int n;
struct Range
{
    int l , r;
    bool operator<(const Range& W) const
    {
        return l < W.l;
    }
}range[N];

int main()
{
    int st , ed;
    cin >> st >> ed >> n;
    for(int i = 0 ; i < n ; i++) cin >> range[i].l >> range[i].r;
    
    sort(range,range+n); //区间排序
    
    int ans = 0;
    bool falg = false; //判断是否包括ed
    for(int i = 0 ; i < n ; i ++)
    {
        int j = i, r = -2E9;
        //找左端点小于st 且右端点最长的区间
        while(j < n && range[j].l <= st)
        {
            r = max(r,range[j].r);
            j++;
        }
        //如果r < st ,说明没有左端点小于st的区间
        if(r < st)
        {
            ans = -1 ;
            break;
        }
        ans ++ ; 
        //如果 r >= end ，说明区间已经覆盖完毕
        if(r >= ed)
        {
            falg = ed;
            break;
        }
        i = j-1; //下一个i从j - 1的区间开始，因为 i - j - 1 的区间已经扫描过了
        st = r; //更新 st作为新的起点
    }
    
    if(!falg) ans = -1;
    cout << ans << endl;
    return 0;
}