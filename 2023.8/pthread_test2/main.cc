#include<iostream>
#include<pthread.h>
#include<unistd.h> 
#include<signal.h>


void* ThreadRoutine(void* args)
{
  //pthread_detach(pthread_self());
  int count = 3;
  while(count--)
  {
    printf("new thread , count = %d\n",count);
    sleep(1);
  }
  return nullptr; //返回自己的id
}

int main()
{
  pthread_t tid; 
  pthread_create(&tid , nullptr, ThreadRoutine,(void*)"new thread ");
  while(1)
  {
    printf("main thread runing.... \n");
    sleep(1);
  }

  return 0 ;
}
