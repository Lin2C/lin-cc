#include "RingQueue.hpp"
#include <time.h>
#include<unistd.h>
#include<string>

#define CONNUM 5 
#define PRODNUM 2



//生产者不断生产任务
void* ProcuderRuning(void* args)
{
    wyl::RingQueue<wyl::Task>* rq = ( wyl::RingQueue<wyl::Task>* )args;
        //wyl::RingQueue<int>* rq = (wyl::RingQueue<int>*)args;
    while(1)
    {
        sleep(1);
        int x = rand() % 10 + 1;
        int y =  rand()%20;
        char op = "+-*/%"[rand() % 5];
        rq->Push(wyl::Task(x,op,y));
        // int x = rand() % 100;
        // rq->Push(x);
        printf("%p 生产了一个任务 ： %d %c %d = ? \n",pthread_self(),x,op,y);
    }
}

void* ConsumerRuning(void* args)
{
    wyl::RingQueue<wyl::Task>* rq = ( wyl::RingQueue<wyl::Task>* )args;
    //wyl::RingQueue<int>* rq = (wyl::RingQueue<int>*)args;
    while(1)
    {

        wyl::Task t;
        //int x; 
        rq->Pop(&t);
        printf("%p 消费了一个任务",pthread_self());
        t.Runing();
    }
}

int main()
{
    pthread_t con[CONNUM]; 
    pthread_t prod[PRODNUM]; 
    srand((unsigned int)0);
    wyl::RingQueue<wyl::Task>* rq = new wyl::RingQueue<wyl::Task>(5);
   // wyl::RingQueue<int>* rq = new wyl::RingQueue<int>(5);

    //创建生产者
    for(int i = 0 ; i < PRODNUM ; i++)
    {
        std::string name = "prodcuer ";
        name += std::to_string(i+1); 
        pthread_create(prod + i,nullptr,ProcuderRuning,(void*)rq);
    }
    
    //创建消费者
    for(int i = 0 ; i < CONNUM ; i++)
    {
        std::string name = "consumer ";
        name += std::to_string(i+1); 
        pthread_create(con + i,nullptr,ConsumerRuning,(void*)rq);
    }
    
    //等待线程
    for(int i = 0 ; i < PRODNUM ; i++)
    {
        pthread_join(prod[i],nullptr);
    }

    for(int i = 0 ; i < CONNUM ; i++)
    {
        pthread_join(con[i],nullptr);
    }

    return 0;
}