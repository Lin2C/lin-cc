#pragma once

#include <iostream>

namespace wyl
{
    class Task{
    public:
        Task(){}
        Task(int x, char op,int y):_x(x),_op(op),_y(y),_iserror(false){}

        void Runing()
        {
            int ret = 0;
            switch(_op)
            {
                case '+' : ret = _x + _y; break; 
                case '-' : ret = _x - _y; break;
                case '*' : ret = _x * _y; break;
                case '/' :
                { 
                    if(_y) ret = _x / _y;
                    else _iserror = true;
                    break;
                }
                case '%' :
                { 
                    if(_y) ret = _x % _y;
                    else _iserror = true;
                    break;
                }
                default: _iserror = true; 
            }
            if(_iserror) std::cout << "result error" << std::endl; 
            else std::cout << _x << _op << _y << "=" << ret << std::endl;
        }
    public:
        int _x;
        char _op;
        int _y;
        bool _iserror;
    };
}