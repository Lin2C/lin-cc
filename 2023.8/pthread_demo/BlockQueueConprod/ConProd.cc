#include "BlockQueue.hpp"
#include <time.h>
#include<unistd.h>
#include<string>

#define CONNUM 5 
#define PRODNUM 2



//生产者不断生产任务
void* ProcuderRuning(void* args)
{
    wyl::block_queue<wyl::Task>* bq = (wyl::block_queue<wyl::Task>*)args;
    while(1)
    {
        int x = rand() % 10 + 1;
        int y =  rand()%20;
        char op = "+-*/%"[rand() % 5];
        bq->Push(wyl::Task(x,op,y));
        sleep(1);

    }
}

void* ConsumerRuning(void* args)
{
    wyl::block_queue<wyl::Task>* bq = (wyl::block_queue<wyl::Task>*)args;
    while(1)
    {
        wyl::Task t; 
        bq->Pop(&t);
        printf("%p 消费了一个任务",pthread_self());
        t.Runing();
    }
}

int main()
{
    pthread_t con[CONNUM]; 
    pthread_t prod[PRODNUM]; 
    srand((unsigned int)0);
    wyl::block_queue<wyl::Task>* bq = new wyl::block_queue<wyl::Task>(5);

    //创建生产者
    for(int i = 0 ; i < PRODNUM ; i++)
    {
        std::string name = "prodcuer ";
        name += std::to_string(i+1); 
        pthread_create(prod + i,nullptr,ProcuderRuning,(void*)bq);
    }
    
    //创建消费者
    for(int i = 0 ; i < CONNUM ; i++)
    {
        std::string name = "consumer ";
        name += std::to_string(i+1); 
        pthread_create(con + i,nullptr,ConsumerRuning,(void*)bq);
    }
    
    //等待线程
    for(int i = 0 ; i < PRODNUM ; i++)
    {
        pthread_join(prod[i],nullptr);
    }

    for(int i = 0 ; i < CONNUM ; i++)
    {
        pthread_join(con[i],nullptr);
    }

    return 0;
}