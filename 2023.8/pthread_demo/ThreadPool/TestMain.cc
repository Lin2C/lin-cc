
#include <iostream> 
#include <ctime>
#include "ThreadPool.hpp"
#include "Task.hpp"
#include "Log.hpp"

using namespace wyl; 

int main()
{
    srand(time(nullptr) ^ getpid());
    ThreadPool<Task> tp; 
    tp.Run();
    while(1)
    {
        int x = rand() % 20 + 1;
        int y = rand() % 20 + 1;
        Task t( x, y, [](int x, int y)->int {return x + y;}); 
        tp.Push(t);
        logMessage(NORMAL,"推送了一个任务 %d + %d = ?",x,y);
        sleep(1);
    }

    return 0 ;
}