#pragma once 

#include <ctime>
#include <iostream>
#include <stdarg.h>
#include <string>
#define DEBUG  0
#define NORMAL 1
#define WARING 2
#define ERROR  3
#define FATAL  4

const char* gLevelMap[]
{
    "DEBUG",
    "NORMAL",
    "WARING",
    "ERROR",
    "FATAL"
};

void logMessage(int level , const char* format,...)
{
#define ifndef DEBUG_SHOW
    if(level == DEBUG) return;
#define endif
    char stdbuff[1024] = {0} ; 
    time_t timestamp = time(nullptr);  //获取时间戳
    struct tm* ltime = localtime(&timestamp);  //获取时间
    //把固定格式写入stdbuff
    snprintf(stdbuff,sizeof stdbuff,"[%s][%d-%d-%d %d:%d:%d][%s][%d] : ",gLevelMap[level],1900+ltime->tm_year
    ,ltime->tm_mon + 1,ltime->tm_mday,ltime->tm_hour,ltime->tm_min,ltime->tm_sec,__FILE__,__LINE__);
    //用户自定义信息buff
    char logbuff[1024] = {0} ; 
    va_list args;  
    va_start(args,format); //提取格式
    vsnprintf(logbuff,sizeof logbuff,format,args);  //把自定义格式写入到logbuff中
    va_end(args);  //关闭args
    printf("%s%s\n",stdbuff,logbuff); 
}
