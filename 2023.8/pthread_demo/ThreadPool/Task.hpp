#pragma once

#include <iostream>
#include <functional> 
#include "Log.hpp"

typedef std::function<int(int,int)> func_t;

namespace wyl
{
    class Task{
    public:
        Task(){}
        Task(int x,int y,func_t func):_x(x),_y(y),_func(func){}

        void operator()()
        {
            logMessage(DEBUG,"处理了一个任务: %d + %d = %d",_x,_y,_func(_x,_y));
        }
    public:
        int _x;
        int _y;
        func_t _func; 
    };
}