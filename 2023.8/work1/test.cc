#include<iostream>

class A
{
public:
    A() : m_iVal(0) { test(); }
    virtual void func() { std::cout << m_iVal <<' ';}
    void test() { func(); }
public:
    int m_iVal;
};

class B : public A
{
public:
    B() { test(); }
    virtual void func()
    {
        ++m_iVal;
        std::cout << m_iVal <<' ';
    }
};

int main(int argc, char *argv[])
{
    A *p = new B;
    p->test();
    std::cout << std::endl;
    return 0;
}
/*
A.1 0
B.0 1
C.0 1 2
D.2 1 0
E.不可预期
F. 以上都不对
*/
