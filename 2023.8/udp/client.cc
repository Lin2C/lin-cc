#include "client.hpp"
#include <memory>

int main(int argc , char* argv[])
{
    if(argc != 3)
    {
        std::cout << "Usage : " << argv[0] << "   serverip  serverport" << std::endl; 
        exit(1);
    }
    uint16_t port = atoi(argv[2]); 
    std::string ip = argv[1];
    std::unique_ptr<UdpClient> cli(new UdpClient(port,ip)); 
    cli->init();
    cli->start();
}