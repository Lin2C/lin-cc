#include "server.hpp"
#include <memory>

void MessageHandle(std::string ip , uint16_t port, std::string message)
{
    //回显数据 
    std::cout << ip << "-" << port << "# " << message << std::endl;
}


int main(int argc , char* argv[])
{
    if(argc != 2) //命令行参数不为2就退出
    {
        std::cout << "Usage : " << argv[0] << "   bindport" << std::endl;  //打印使用手册
        exit(1);
    }
    uint16_t port = atoi(argv[1]); //命令行传的端口转换成16位整形
    std::unique_ptr<UdpServer> s(new UdpServer(port,MessageHandle)); //创建UDP服务器
    s->init(); //初始化服务器，创建 + 绑定
    s->start(); //运行服务器
}