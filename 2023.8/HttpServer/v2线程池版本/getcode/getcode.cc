#include<fcntl.h>
#include<sys/stat.h>
#include<unistd.h>
#include<iostream>
#include<string> 
#include<time.h>

#define CODE_TXT "./code.txt"

int main()
{
    srand(time(0));
    int fd = open(CODE_TXT,O_RDWR | O_APPEND | O_CREAT,0666);
    std::string str_cod;
    int count = 20;
    while(count--)
    { 
        for(int i = 0 ; i < 16;i++)
        {
            int a = (rand() ^ getpid()) % 36 ;
            if(a < 10) a += '0';
            else a += 'A' - 10;
           str_cod += a;
        }
        str_cod += "\n";
    }
    write(fd,str_cod.c_str(),str_cod.size());
    close(fd);
}
