#pragma once
#include "HandlerData.hpp"
#include "TcpServer.hpp"


namespace wyl{

    class HttpServer
    {
    public:
        HttpServer(func_t hdl,uint16_t port = 8080) : _tcp(new TcpServer(hdl,port)){}
        ~HttpServer(){delete _tcp;}

        void init(){_tcp->init();}
        void start(){_tcp->start();}
    private:
        TcpServer* _tcp;
        Request _req;
        Response _resp;
    };

}