#pragma once 

#include <pthread.h>
#include <string> 

namespace wyl
{
    typedef void*(*routine_t)(void*); 

    class ThreadDate
    {
    public:
        std::string _name; 
        void *_args; 
    };

    class Thread
    {
    public:
        Thread(int num , routine_t routine,void* args):_routine(routine){
            _name = "Thread-" + std::to_string(num); 
            _td._name = _name;
            _td._args = args;
        }   

        void Start()
        {
            pthread_create(&_tid,nullptr,_routine,(void*)&_td);
        }

        void Join()
        {
            pthread_join(_tid,nullptr);
        }
         
        std::string GetName()
        {
            return _name;
        }

    private: 
        pthread_t _tid;
        std::string _name; 
        routine_t _routine; 
        ThreadDate _td;
    };

};