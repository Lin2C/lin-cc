<% page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=0">  
    <meta name="format-detection" content="telephone=no" />  
    <meta name="apple-mobile-web-app-capable" content="yes" />  
    <meta name="apple-mobile-web-app-status-bar-style" content="black">   
    
    <title>注册</title>

    <link rel="stylesheet" href="./css/base.css">
    <link rel="stylesheet" href="./css/register.css">
    <link rel="stylesheet" href="./js/register.js">

</head>
<body class="body1">
   <div class="login-form">
       <div class="login-items">
            <div><h2>用户注册</h2></div>
            <form action="">
            <div class="login-form-center">
                <div class="form-input">
                    <label ><input type="text" name="uname" placeholder="请输入姓名">
                    <span class="msg"></span>
                </div>
                <div class="form-input">
                    <label ><input type="text" name="unum" placeholder="请输入学号/教职工号">
                    <span class="msg"></span>
                </div>
                <div class="form-input">
                    <input type="password" name="password" id="password" placeholder="请输入6到8位密码">
                    <span class="msg"></span>
                </div>
                <div class="form-input">
                    <input type="password" name="confirm" id="password1" placeholder="请再次输入密码">
                    <span class="msg"></span>
                </div>
                <div class="form-input">
                    <input type="text" name="email" placeholder="请输入电子邮箱">
                    <span class="msg"></span>
                </div>
                <div class="form-input">
                    <input type="text" name="phone" placeholder="请输入电话号码">
                    <span class="msg"></span>
                </div>
                <div class="form-input">
                    <button type="submit" name="regist">注册</button>
                    <button type="reset" name="refill">重置</button>
                </div>
            </div>
            </form>
        </div>
    </div>

    <script>
         // 获取姓名
 const uname =document.querySelector('[name=uname]') 
 uname.addEventListener('change',verifyUname)
 function verifyUname(){
     const span = uname.nextElementSibling
     const reg = /^[\u00B7\u3007\u3400-\u9FFF\uE000-\uF8FF\uF900-\uFAFF\u{20000}-\u{2FFFF}\u{30000}-\u{3FFFF}]+$/u 
     if(!reg.test(uname.value)){
         span.innerHTML = '请输入正确姓名'
         return false
     }
     span.innerHTML = ''
     return true
 }

 // 获取学号/教职工号
 const unum =document.querySelector('[name=unum]') 
 unum.addEventListener('change',verifyUnum)
 function verifyUnum(){
     const span = unum.nextElementSibling
     const reg =  /^[A-Z0-9_]{6,8}$/
     if(!reg.test(unum.value)){
         span.innerHTML = '请输入正确学号/教职工号'
         return false
     }
     span.innerHTML = ''
     return true
 }


 // 获取密码
 const psd = document.querySelector('[id=password]')
 psd.addEventListener('change',verifyPwd)
 function verifyPwd(){
     const span = psd.nextElementSibling
     const reg = /^[a-zA-Z0-9_]{6,8}$/
     if(!reg.test(psd.value)){
         span.innerHTML = '输入不合法,请输入6~8位密码'
         return false
     }
     span.innerHTML = ''
     return true
 }

 // 确认密码
 const confirm = document.querySelector('[id=password1]')
 confirm.addEventListener('change',verifyConfirm)
 function verifyConfirm(){
     const span = confirm.nextElementSibling
     if (password1.value != password.value){
         span.innerHTML = '两次密码输入不一致'
         return false
     }
     span.innerHTML = ''
     return true
 }

 // 手机号
 const phone = document.querySelector('[name=phone]')
 phone.addEventListener('change',verifyPhone)
 function verifyPhone(){
     const span = phone.nextElementSibling
     const reg = /^1(3\d|4[5-9]|5[0-35-9]|6[567]|7[0-8]|8\d|9[0-35-9])\d{8}$/
     if(!reg.test(phone.value)){
         span.innerHTML = '输入不合法,请输入正确的电话号码'
         return false
     }
     span.innerHTML = ''
     return true
 }


 // 邮箱
 const email = document.querySelector('[name=email]')
 email.addEventListener('change',verifyEmail)
 function verifyEmail(){
     const span = email.nextElementSibling
     const reg = /^([a-zA-Z0-9_-])+@([a-zA-Z0-9_-])+(.[a-zA-Z0-9_-])+/
     if(!reg.test(email.value)){
         span.innerHTML = '输入不合法,请输入正确的电子邮箱'
         return false
     }
     span.innerHTML = ''
     return true
 }


 // 提交注册
 const form = document.querySelector('form')
 form.addEventListener('submit', function (e) {
     if (!verifyName()) e.preventDefault()
     if (!verifyPhone()) e.preventDefault()
     if (!verifyCode()) e.preventDefault()
     if (!verifyPwd()) e.preventDefault()
     if (!verifyConfirm()) e.preventDefault()
 })
    </script>
</body>
</html>