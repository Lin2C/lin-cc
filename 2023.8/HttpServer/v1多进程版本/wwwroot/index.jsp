<% page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>登录</title>
    <link rel="stylesheet" href="./css/base.css">
    <link rel="stylesheet" href="./css/index.css">
    <link rel="stylesheet" href="./js/index.js">
</head>
<body>
    <div class="content"> 
        <div class="login-wrapper">
            <div class="right-login-form">
                <div class="form-wrapper">
                    <h1>登录窗口</h1>
                    <div class="input-items">
                        <span class="input-tips">
                            学号/教职工号
                        </span>
                        <input type="text" name="unum" class="inputs" placeholder="请输入学号/教职工号">
                        <span class="msg"></span>
                    </div>
                    <div class="input-items">
                        <span class="input-tips">
                            密码
                        </span>
                        <input type="password" name="password" id="password" class="inputs" placeholder="请输入6~8密码">
                        <span class="msg"></span>
                    </div>
                    <button class="btn">登录</button>
                    <div class="siginup-tips">
                        <span><a href="./register.html">注册</a></span>
                    </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        // 获取学号/教职工号
const unum =document.querySelector('[name=unum]') 
unum.addEventListener('change',verifyUnum)
function verifyUnum(){
    const span = unum.nextElementSibling
    const reg =  /^[A-Z0-9_]{6,8}$/
    if(!reg.test(unum.value)){
        span.innerHTML = '请输入正确学号/教职工号'
        return false
    }
    span.innerHTML = ''
    return true
}

// 获取密码
const psd = document.querySelector('[id=password]')
psd.addEventListener('change',verifyPwd)
function verifyPwd(){
    const span = psd.nextElementSibling
    const reg = /^[a-zA-Z0-9_]{6,8}$/
    if(!reg.test(psd.value)){
        span.innerHTML = '输入不合法,请输入6~8位密码'
        return false
    }
    span.innerHTML = ''
    return true
}
    </script>
</body>
</html>