#pragma once
#include <iostream>
#include <functional>
#include "Log.hpp"
#include <unistd.h>
#include "HandlerData.hpp"

namespace wyl
{

    typedef std::function<void(const Request &req, Response &resp)> func_t;
    typedef std::function<void(int sock, std::string &in_buff, func_t handler)> func_task_t;

    void HandlerEntry(int sock, std::string &in_buff, func_t handler)
    {
        // 表示层，对http请求进行解析
        Request _req;
        Response _resp(sock);
        bool ret = _req.Parse(in_buff); // 解析请求
        if (ret == false)
        {
            // 解析失败....0
            logMessage(WARING, "request paesed failed....");
            _resp.SetErrCode(400);
            _resp.SetBody(_req.Path());
            _resp.Send();
            return;
        }
        // 处理请求
        logMessage(NORMAL, "begin handler ....");
        // 如果数据需要用户处理
        if (_req.IsHandler())
        {
            handler(_req, _resp);
            // 成功添加后加入到数组
        }
        else
        {
            _resp.SetHeader("Content-Type", _mime_msg[_req.GetSuffix()]);
            _resp.SetBody(_req.Path());
            _resp.Send();
        }

        // 剩下的事情交给用户去做

        // _resp.SetHeader("Content-Type",_mime_msg[_req.GetSuffix()]);

        // //构建响应
        // std::string messg = _resp.BuildResponse(_req.Path());
        // //发送响应
        // _resp.Send(sock,messg); //接收发送数据的大小
    }

    class Task
    {
    public:
        Task() {}
        Task(int sock, const std::string &inbuff, func_t handler,func_task_t cb) :
         _sock(sock), _inbuff(inbuff), _handler(_handler), _cb(cb) {}

        void operator()()
        {
            _cb(_sock, _inbuff, _handler);
        }

    public:
        int _sock;
        std::string _inbuff;
        func_t _handler;
        func_task_t _cb;
    };
}