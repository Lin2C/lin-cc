#include<iostream>
#include<pthread.h>
#include<unistd.h>
#include<sys/wait.h>
#include<sys/types.h>
#include<fcntl.h>

void *routine(void* args)
{
    int pid = fork(); 
    if(pid == 0)
    {
        int cnt = 10;
        while(cnt--)
        {
            sleep(1);
            std::cout << "i am thread child,cnt = "<< cnt << std::endl;
        }
        exit(0);
    }
    waitpid(pid,nullptr,0); 
    return nullptr;
}


int main()
{

    pthread_t tid;
    pthread_create(&tid,nullptr,routine,nullptr);
    while(1)
    {
        std::cout << "i am main thread" << std::endl;
        sleep(1);
    }

}