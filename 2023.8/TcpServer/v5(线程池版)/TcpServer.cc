
#include "TcpServer.hpp"
#include "deamon.hpp"

int main(int argc , char* argv[])
{
    if(argc != 2)
    {
        std::cout << "Usage :  " << argv[0] << "  serverport" << std::endl;
        exit(USAG_ERR);
    }
    //提取端口
    uint16_t port = atoi(argv[1]);
    wyl::TcpServer tcpserver(port); 
    tcpserver.init();
    daemonSelf();
    tcpserver.start();

    return 0 ;
}