#include "deamon.hpp"
#include <iostream>
#include <string>
#include <unistd.h>

int main()
{
    daemonSelf(); //独立进程组
    FILE* testTxt = fopen("test.txt","a"); 
    int i = 0;
    while(1)
    {
        std::string msg = "hello" + std::to_string(i);
        fprintf(testTxt,msg.c_str());
        sleep(1);
    }

    return 0 ;
}