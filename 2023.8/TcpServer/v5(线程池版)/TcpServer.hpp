#pragma once 
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include<unistd.h>
#include <iostream>
#include <string>
#include <pthread.h>
#include "ThreadPool.hpp" 
#include "Task.hpp"

#include "Log.hpp"

enum {
    USAG_ERR = 1,
    SOCK_ERR,
    BIND_ERR,
    LSITEN_ERR
};
#define PORT 8080

namespace wyl
{
    class TcpServer;
    class ThreadData
    {
    public:
        TcpServer* _tcp; 
        int _sock;

        ThreadData(TcpServer* tcp,int sock) :_tcp(tcp),_sock(sock){}
    };

    class TcpServer
    {
    public:
        TcpServer(uint16_t port = PORT) : _list_sock(-1),_port(port){}
        ~TcpServer(){close(_list_sock);}

        void init()
        {
            //1.创建套接字
            _list_sock = socket(AF_INET,SOCK_STREAM,0);
            if(_list_sock < 0)
            {
                logMessage(FATAL,"listsock created fail: %d",_list_sock);
                exit(SOCK_ERR);
            }
            logMessage(NORMAL,"listsock created success: %d",_list_sock);

            //2.绑定套接字
            struct sockaddr_in svr;
            svr.sin_port = htons(_port); 
            svr.sin_addr.s_addr = INADDR_ANY;
            svr.sin_family = AF_INET; 
            if(bind(_list_sock,(struct sockaddr*)&svr,sizeof svr) != 0)
            {
                logMessage(FATAL,"listsock bind fail");
                exit(BIND_ERR);
            }
            logMessage(NORMAL,"listsock bind success: %d",_list_sock);

            //3.监听套接字
            if(listen(_list_sock,5) != 0)
            {
                logMessage(FATAL,"listsock listen fail");
                exit(LSITEN_ERR);
            }
            logMessage(NORMAL,"listsock listen success: %d",_list_sock);
        }

        void start()
        {
            while(1)
            {
                //Accept获取连接，没有连接到来则阻塞
                struct sockaddr_in peer;
                socklen_t peer_len = sizeof peer;
                int new_sock = accept(_list_sock,(struct sockaddr*)&peer,&peer_len);
                if(new_sock < 0)
                {
                    logMessage(WARING,"listsock accept fail");
                    continue;
                }
                logMessage(NORMAL,"accept success  new sock : %d",new_sock);
                //把任务放进线程池
                ThreadPool<Task>* _tps = ThreadPool<Task>::GetInstance();
                _tps->Push(Task(new_sock,IOServer));
            }
        }

    private:
        int _list_sock; 
        int _port;
    };
};