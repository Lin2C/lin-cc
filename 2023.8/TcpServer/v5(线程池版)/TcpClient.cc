#include "TcpClient.hpp"

int main(int argc, char* argv[])
{
    if(argc != 3)
    {
        std::cout << "Usage :  " << argv[0] << " serverip  serverport" << std::endl;
        exit(-1);
    }
    uint16_t port = atoi(argv[2]);
    std::string ip = argv[1];
    wyl::TcpClient clinet(ip,port);
    clinet.init();
    clinet.Runing();
    return 0 ;
}