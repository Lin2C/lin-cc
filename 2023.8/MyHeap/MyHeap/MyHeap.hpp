#pragma once
#include <cassert>
#include <algorithm>
namespace wyl
{
	//建小堆
	template<class T>
	struct Greater
	{
		bool operator()(T& x, T& y)
		{
			return x > y;
		}
	};
	//建大堆
	template<class T>
	struct Less
	{
		bool operator()(T& x, T& y)
		{
			return x < y;
		}
	};

	template<class T,class Functor = Less<T>>
	class Heap
	{
	public:
		T* _hp;
		size_t _size;
		size_t _capacity;
		
	public:
		Heap(size_t capacity = 16):_capacity(capacity),_size(0){
			_hp = new T[_capacity];
		}
		~Heap() {
			delete _hp;
			_size = _capacity = 0;
		}
	private:
		void UpDateCapacity()
		{
			T* tmp = new T[_capacity * 1.5];  //开辟一个新的堆
			assert(tmp);
			memcpy(tmp, _hp, _size * sizeof(T));
			std::swap(tmp, _hp);
			_capacity = _capacity * 1.5;
		}
		//交换两个数的值
		void swap(T& x, T& y)
		{
			T tmp = x; 
			x = y; 
			y = tmp;
		}
		void Up()
		{
			Functor func;
			//从最后一个开始向上调整
			int child = _size - 1; 
			int parent = (child - 1) / 2; 
			while (child) //child != 0 
			{
				//大堆
				if (func(child,parent))
				{
					swap(_hp[child], _hp[parent]);
					child = parent;
					parent = (child - 1) / 2;
				}
				else break;
			}
		}
		void Down()
		{
			Functor func;
			int parent = 0;
			int child = parent * 2 + 1; 
			while (parent < _size)
			{
				if (child + 1 >= _size - 1 && func(_hp[child + 1],_hp[child]))
					child++; //如果右孩子存在且右孩子 > 左孩子，那么child变成右孩子
				if (child >= _size - 1) break; //左孩子不存在
				if (func(_hp[child],_hp[parent]))
				{
					swap(_hp[parent], _hp[child]);
					parent = child;
					child = parent * 2 + 1;
				}
				else break;
			}
		}
	public:
		void push(const T& val)
		{
			if (_size == _capacity)
			{
				//扩容 
				UpDateCapacity();
			}
			_hp[_size++] = val;
			Up(); 
		}
		T top()
		{
			assert(_size > 0);
			return _hp[0];
		}
		void pop()
		{
			assert(_size > 0);
			//根节点和最后一个节点交换
			swap(_hp[0], _hp[_size-- - 1]);
			Down();
		}
	};

	void HeapTest1()
	{
		//push向上调整测试
		Heap<int> hp;
		hp.push(10);
		hp.push(15);
		hp.push(25);
		hp.push(30);
		hp.push(50);
		hp.push(70);
		hp.push(70);

	}

	void HeapTest2()
	{
		//push向上调整测试
		Heap<int,Less<int>> hp(2);
		hp.push(10);
		hp.push(15);
		hp.push(25);
		hp.push(30);
		hp.push(50);
		hp.push(70);
		hp.push(70);
		hp.push(70);

		hp.pop();
		hp.pop();
		hp.pop();
		hp.pop();
		hp.pop();
		hp.pop();		

	}
}
