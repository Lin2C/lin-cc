#include "server.hpp"
#include <memory>
#include <unistd.h>
#include <fcntl.h>
#include <vector>
#include <sys/wait.h>
#include <cstring>
#include "User.hpp"

UserManage um;

void ChatRoomMessage(int _sock,std::string ip,uint16_t port,std::string message)
{
    //如果用户输入online，那么就把用户添加到在线列表
    if(message == "online") um.online(port,ip); 
    //如果用户输入offline,那么把用户移除在线列表
    if(message == "offline") um.offline(port,ip);
    //用户在线才能广播消息
    if(um.isonline(port,ip))    
        um.broadcastMessage(message,_sock,ip,port); //广播消息
}

int main(int argc , char* argv[])
{
    if(argc != 2) //命令行参数不为2就退出
    {
        std::cout << "Usage : " << argv[0] << "   bindport" << std::endl;  //打印使用手册
        exit(1);
    }
    uint16_t port = atoi(argv[1]); //命令行传的端口转换成16位整形
    std::unique_ptr<UdpServer> s(new UdpServer(port,ChatRoomMessage)); //创建UDP服务器
    s->init(); //初始化服务器，创建 + 绑定
    s->start(); //运行服务器
}