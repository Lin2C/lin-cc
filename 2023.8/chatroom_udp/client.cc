#include "client.hpp"
#include <memory>

int main(int argc , char* argv[])
{
    if(argc != 3) //必须在命令行输入：./clinet 服务器ip 服务器端口   才可以启动客户端
    {
        std::cout << "Usage : " << argv[0] << "   serverip  serverport" << std::endl; 
        exit(1);
    }
    uint16_t port = atoi(argv[2]); //提取服务器端口
    std::string ip = argv[1]; //提取服务器ip
    std::unique_ptr<UdpClient> cli(new UdpClient(port,ip));  //创建客户端
    cli->init(); //客户端初始化
    cli->start(); //客户端runing....
}