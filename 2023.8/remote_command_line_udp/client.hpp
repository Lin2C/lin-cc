#pragma once
#include <string>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <unistd.h>
#include <iostream>
#include <arpa/inet.h>
#include <cstdio>
#include <cstring>

class UdpClient
{
public: 
    UdpClient(uint16_t port , const std::string& ip) : _port(port), _svr_ip(ip){}
    ~UdpClient(){ close(_sock); }

    void init()
    {
         _sock = socket(AF_INET,SOCK_DGRAM,0); 
        if(_sock < 0)
        {
            std::cout << "create socket failed...." << std::endl;
            abort();
        }   
        svr.sin_port = htons(_port); 
        svr.sin_addr.s_addr = inet_addr(_svr_ip.c_str()); 
        svr.sin_family = AF_INET;

    }
    void start()
    {
        int i = 1; 
        char sendbuff[1024] = {0};
        while(1)
        {
            //输入命令行
            std::cout << "[XXXX@abcdefg]$ ";
            fgets(sendbuff,sizeof sendbuff -1 , stdin); 
            sendbuff[strlen(sendbuff) - 1] = 0;
            std::string message = sendbuff;   
            //发送命令信息
            sendto(_sock,message.c_str(),message.size(),0,(struct sockaddr*)&svr,sizeof svr);
            //收服务器请求
            char recvbuff[1024 * 4] = {0};
            recvfrom(_sock,recvbuff,sizeof recvbuff - 1,0,nullptr,nullptr);
            //打印回收到的消息
            std::cout << recvbuff;
        }
    }

private: 
    int _sock;
    uint16_t _port;
    std::string _svr_ip;  
    struct sockaddr_in svr;

};