#include<iostream> 
#include<pthread.h> 
#include<unistd.h> 


int x = 0;

void* ThreadRun(void* name)
{
  while(1)
  {
    std::cout << "this is " << (char*)name << ", pid = " << getpid() << "  x = " << x  << std::endl;
    sleep(1);
  }
}

int main()
{
  pthread_t tid; 
  pthread_create(&tid,nullptr,ThreadRun,(void*)"new thread");
  while(1)
  {
    x++;
    std::cout << "this is main thread , pid = " << getpid() <<"  x = " << x << std::endl;
    sleep(1);
  }
}
